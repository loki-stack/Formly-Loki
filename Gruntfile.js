module.exports = function (grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),



    copy: {
      main: {
        expand: true,
        cwd: '.',
        src: ['app-data/**/*', 'assets/**/*', 'libs/**/*', 'index.html'],
        dest: 'dist/',
      },
    },
    ngAnnotate: {
      options: {
        singleQuotes: true
      },
      app: {
        files: [{
          expand: true,
          src: ['dist/app-data/**/*.js'],
          dest: 'dist',
          cwd: '.',
          rename: function (dst, src) {
            // To keep src js files and make new files as *.min.js :
            // return dst + '/' + src.replace('.js', '.js');
            // Or to override to src :
            return src;
          }
        }]
      }
    },
    uglify: {
      dev: {
        options: {
          mangle: true
        },
        files: [{
          expand: true,
          src: ['dist/app-data/**/*.js'],
          dest: 'dist',
          cwd: '.',
          rename: function (dst, src) {
            // To keep src js files and make new files as *.min.js :
            // return dst + '/' + src.replace('.js', '.js');
            // Or to override to src :
            return src;
          }
        }]
      }
    },
    jshint: {
      all: ['Gruntfile.js', 'views/cms/cmspage/interface-builder/page-content.js']
    }
  });



  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-ng-annotate');
  // Default task(s).
  grunt.registerTask('default', ['copy', 'ngAnnotate', 'uglify']);
  grunt.registerTask('copy', ['copy']);
  grunt.registerTask('ngAnnotate', ['ngAnnotate']);
  grunt.registerTask('uglify', ['uglify']);

};
