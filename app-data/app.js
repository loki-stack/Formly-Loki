﻿define(['angularAMD', 'jquery', 'ag-grid',
    'components/config/routeconfig',
    "components/formly-template/formly-template",
    'views/sidebar/sidebar-controller',
    'views/quick-sidebar/quick-sidebar',
    "views/header/header",
    "views/footer/footer",
    'components/service/amdservice',
    'components/directive/stop-propagation-directive',
    'components/service/timeoutservice',
    'angular-formly',
    'angular-formly-templates-bootstrap',
    'bootstrap',
    'jwplayer',
    'angular-route',
    'angular-cookies',
    'angular-loading-bar',
    'angular-sanitize',
    'angular-messages',
    'ng-ckeditor',
    'ngtaginput',
    'ui-bootstrap',
    'ui-select',
    'ngfileupload',
    'tmhDynamicLocale',
    'angular-translate',
    'angular-translate-loader-partial',
    'angular-translate-storage-cookie',
    'angular-translate-storage-local',
    'socketio', 'angular-socket-io',
], function (angularAMD, jquery, agGrid, routeConfig, formlyConfig, sidebarCtrl, quickSidebarCtrl, headerCtrl, footerCtrl) {
    'use strict';

    agGrid.initialiseAgGridWithAngular1(angular);
    // Declare app level module which depends on views, and components
    var app = angular.module('myApp', [
        'ngRoute',
        'ngCookies',
        'angular-loading-bar',
        'ngSanitize',
        'ngMessages',
        'ngCkeditor',
        'ngTagsInput',
        'ui.bootstrap',
        'ui.select',
        'ngFileUpload',
        'tmh.dynamicLocale',
        'pascalprecht.translate',
        'formly',
        'formlyBootstrap',
        'socket.io',
        "agGrid",
    ]);
    angularAMD.Version = "1.00";
    app.factory('settings', ['$rootScope', function ($rootScope) {
        var settings = {
            layout: {
                pageSidebarClosed: false, // sidebar menu state
                pageContentWhite: true, // set page content layout
                pageBodySolid: false, // solid body color state
                pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
            },
            assetsPath: '../assets',
            globalPath: '../assets/global',
            layoutPath: '../assets/layouts/layout',
        };

        $rootScope.settings = settings;

        return settings;
    }]);
    /*Build performance config*/
    app.config(['$compileProvider', function ($compileProvider) {
       $compileProvider.debugInfoEnabled(false);
    }]); 
    app.config(function ($httpProvider) {
       $httpProvider.useApplyAsync(true);
    });
    app.run(['$templateCache', function ($templateCache) {
        $templateCache.put('searchbox.tpl.html', '<input id="pac-input" class="pac-controls" type="text" ng-model="ngModel" placeholder="Search">');
        $templateCache.put('window.tpl.html', '<div ng-controller="WindowCtrl" ng-init="showPlaceDetails(parameter)">{{place.name}}</div>');
    }]);
    /*Translate config*/
    app.config(['$translateProvider', '$translatePartialLoaderProvider', function ($translateProvider, $translatePartialLoaderProvider) {
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: '../app-data/{part}/i18n/lang_{lang}.json'
        });
        $translatePartialLoaderProvider.addPart('views/header');
        $translatePartialLoaderProvider.addPart('views/sidebar');
        $translatePartialLoaderProvider.addPart('views/footer');
        $translateProvider.preferredLanguage('vi');
        $translateProvider.useLocalStorage();
    }]);

    app.config(['tmhDynamicLocaleProvider', function (tmhDynamicLocaleProvider) {
        tmhDynamicLocaleProvider.localeLocationPattern('../libs/angular/i18n/angular-locale_{{locale}}.js');
        tmhDynamicLocaleProvider.defaultLocale('vi');
    }]);
    /*Loadingbar config*/
    app.config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
        cfpLoadingBarProvider.includeSpinner = true;
        cfpLoadingBarProvider.latencyThreshold = 100;

    }]); 

    // Init config header footer sidebar 
    routeConfig.setupRouteConfig(app, angularAMD)
    headerCtrl.init(app);
    sidebarCtrl.init(app);
    quickSidebarCtrl.init(app);
    footerCtrl.init(app);
    formlyConfig.init(app);
    // Hide loading
    jquery("#loadingDiv").css('display', 'none');
    app.directive('dateutc', function () {
        return {
            restrict: 'A',
            priority: 1,
            require: 'ngModel',
            link: function (scope, element, attrs, ctrl) {
                ctrl.$formatters.push(function (value) {
                    try {
                        var date = new Date(Date.parse(value));
                        date = new Date(date.getTime());
                        return date;
                    } catch (e) {
                        return null;
                    }
                });

                ctrl.$parsers.push(function (value) {
                    if (value !== null && typeof (value) !== undefined) {
                        var date = new Date(value.getTime() - (60000 * value.getTimezoneOffset()));
                        return date;
                    } else {
                        return null;
                    }

                });
            }
        };
    });
    app.directive('timpickerutc', function () {
        return {
            restrict: 'A',
            priority: 1,
            require: 'ngModel',
            link: function (scope, element, attrs, ctrl) {
                ctrl.$formatters.push(function (value) {
                    try {
                        var date = new Date(Date.parse(value));
                        date = new Date(date.getTime());
                        return date;
                    } catch (e) {
                        return null;
                    }
                });

                ctrl.$parsers.push(function (value) {
                    if (value !== null && typeof (value) !== undefined) {
                        var date = new Date(value.getTime() - (60000 * value.getTimezoneOffset()));
                        return date;
                    } else {
                        return null;
                    }
                });
            }
        };
    });
    app.directive('datepickerutc', function () {
        return {
            restrict: 'A',
            priority: 1,
            require: 'ngModel',
            link: function (scope, element, attrs, ctrl) {
                ctrl.$formatters.push(function (value) {
                    //if (value !== null && typeof (value) !== undefined) {
                    //    try {
                    //        var date = new Date(Date.parse(value));
                    //        date = new Date(date.getTime() - (60000 * date.getTimezoneOffset()));
                    //        return date;
                    //    } catch (e) {
                    //        return null;
                    //    } 
                    //} else {
                    //    return null;
                    //}
                    return value;
                });

                ctrl.$parsers.push(function (value) {
                    if (value !== null && typeof (value) !== undefined) {
                        var date = new Date(value.getTime() - (60000 * value.getTimezoneOffset()));
                        return date;
                    } else {
                        return null;
                    }
                });



            }
        };
    });
    app.filter('highlightfts', function () {
        function removeVietNamSign(str) {
            str = str.toLowerCase();
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
            str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
            str = str.replace(/đ/g, "d");
            //str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "-");
            //str = str.replace(/-+-/g, "-"); //thay thế 2- thành 1- 
            // str = str.replace(/^\-+|\-+$/g, "");
            return str;
        }
        return function (matchItem, query) {
            if (query && matchItem) {
            var queryNS = removeVietNamSign(query);
            var matchItemNS = removeVietNamSign(matchItem);
                //get các vị trí
                var listItem = matchItemNS.split(queryNS);
                var result = "";
                var fromIndex = 0
                var toIndex = 0
                for (var i = 0; i < listItem.length; i++) {
                    fromIndex = toIndex;
                    toIndex = fromIndex + listItem[i].length;
                    result = result + matchItem.substring(fromIndex, toIndex);
                    fromIndex = toIndex;
                    if (i !== listItem.length - 1) {
                        result = result + '<span class="highlight-fts">';
                        toIndex = fromIndex + queryNS.length;
                        result = result + matchItem.substring(fromIndex, toIndex);
                        result = result + '</span>';
                    }
                }
                return result;
            } else {
                return matchItem;
            }
        };
    })
    return angularAMD.bootstrap(app);
});
