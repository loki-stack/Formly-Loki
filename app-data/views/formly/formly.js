﻿define(['jquery', 'app',
    'jquery-ui',
    'slimscroll',
    'components/factory/factory',
    'components/formly-template/formly-factory',
    'angular-sanitize',
    'views/formly/formly-edit-label',
    'views/formly/formly-edit-control',
    'views/formly/formly-edit-row',
], function (jQuery, app) {
    app.controller('FormlyCtrl', ['$scope', '$sce', '$timeout', '$log', '$http', '$uibModal', 'constantsFactory', 'constantsAMD', '$routeParams', 'FormlyFactory', 'FormlyService',
        function ($scope, $sce, $timeout, $log, $http, $uibModal, constantsFactory, constantsAMD, $routeParams, FormlyFactory, FormlyService) {
            //khai báo các template
            var template = {
                editLabelUrl: '/app-data/views/formly/formly-edit-label.html',
                editControlUrl: '/app-data/views/formly/formly-edit-control.html',
                editRowUrl: '/app-data/views/formly/formly-edit-row.html'
            };

            //khai báo các controller
            var controller = {
                editLabelCtr: 'FormlyEditLabel',
                editControlCtr: 'FormlyEditControl',
                editRowCtr: 'FormlyEditRow'
            };
            /*------------------------------------------------------------------**/
            $scope.ListAvailAbleDbType = FormlyFactory.ListAvailAbleDbType;
            $scope.ListAvailAbleControl = FormlyFactory.ListAvailAbleControl;
            $scope.ListAvailAbleClass = FormlyFactory.ListAvailAbleClass;
            /*-----------------------------------------------------------*/
            /*START common function-----------------------------------------------------------*/
            $scope.deliberatelyTrustDangerousSnippet = function (html) {
                return $sce.trustAsHtml(html);
            };
            /*END common function-----------------------------------------------------------*/
            /*START declare variable..........................................................*/
            $scope.IsShowInfo = false;
            var selectingItem = {
                data: {}
            };
            $scope.FormMode = 'config';
            $scope.vm = {};
            $scope.vm.Data = {};
            $scope.vm.Model = [];
            $scope.vm.Scheme = [];
            $scope.component = [];


            /*Định danh level 1------------------------------------------------------------*/
            var label = {
                "data": {
                    "child": [{
                        "className": "col-md-12  text-center",
                        "data": {
                            "name": "Custom block text",
                            "type": 2
                        },
                        "template": "Please edit text"
                    }],
                    "hasChild": true,
                    "level": 0,
                    "name": "Label",
                    "imageSrc": "/assets/image/block-text.svg"
                }
            };
            var control = {
                "data": {
                    "child": [],
                    "hasChild": true,
                    "level": 0,
                    "name": "Control",
                    "imageSrc": "/assets/image/control.svg"
                }
            };
            /*Định danh level 2------------------------------------------------------------*/
            var input = {
                "data": {
                    "child": [],
                    "hasChild": true,
                    "level": 1,
                    "name": "Input",
                    "imageSrc": "/assets/image/input.svg"
                }
            };
            var textArea = {
                "data": {
                    "child": [],
                    "hasChild": true,
                    "level": 1,
                    "name": "Text-Area",
                    "imageSrc": "/assets/image/text-area.svg"
                }
            };
            var dropdown = {
                "data": {
                    "child": [],
                    "hasChild": true,
                    "level": 1,
                    "name": "Dropdown",
                    "imageSrc": "/assets/image/dropdown.svg"
                }
            };
            var checkbox = {
                "data": {
                    "child": [],
                    "hasChild": true,
                    "level": 1,
                    "name": "Checkbox",
                    "imageSrc": "/assets/image/radio.svg"
                }
            };
            var upload = {
                "data": {
                    "child": [],
                    "hasChild": true,
                    "level": 1,
                    "name": "Upload",
                    "imageSrc": "/assets/image/upload.svg"
                }
            };
            var custom = {
                "data": {
                    "child": [],
                    "hasChild": true,
                    "level": 1,
                    "name": "Custom",
                    "imageSrc": "/assets/image/custom.svg"
                }
            };

            /*Bind level------------------------------------------------------------*/

            input.data.child.push(FormlyFactory.InputTextControl);
            input.data.child.push(FormlyFactory.InputEmailControl);
            input.data.child.push(FormlyFactory.InputCodeControl);
            input.data.child.push(FormlyFactory.InputUrlControl);
            input.data.child.push(FormlyFactory.InputPasswordControl);
            input.data.child.push(FormlyFactory.InputColorControl);
            input.data.child.push(FormlyFactory.InputNumberControl);
            input.data.child.push(FormlyFactory.InputRangeControl);
            input.data.child.push(FormlyFactory.InputDateFullControl);
            input.data.child.push(FormlyFactory.InputDateControl);
            input.data.child.push(FormlyFactory.InputYearControl);
            input.data.child.push(FormlyFactory.InputMonthControl);
            input.data.child.push(FormlyFactory.InputTimeControl);
            /*Bind level------------------------------------------------------------*/
            textArea.data.child.push(FormlyFactory.TextAreaControl);
            textArea.data.child.push(FormlyFactory.CkeditorControl);
            /*Bind level------------------------------------------------------------*/
            dropdown.data.child.push(FormlyFactory.SelectControl);
            dropdown.data.child.push(FormlyFactory.SelectMultiControl);
            dropdown.data.child.push(FormlyFactory.SelectTreeControl);

            /*Bind level------------------------------------------------------------*/
            checkbox.data.child.push(FormlyFactory.RadioButtonControl);
            checkbox.data.child.push(FormlyFactory.CheckboxMultiControl);
            checkbox.data.child.push(FormlyFactory.CheckboxMultiTreeControl);
            checkbox.data.child.push(FormlyFactory.CheckboxControl);
            checkbox.data.child.push(FormlyFactory.ToggleControl);
            /*Bind level------------------------------------------------------------*/
            upload.data.child.push(FormlyFactory.UploadControl);
            upload.data.child.push(FormlyFactory.UploadMultiControl);
            /*Bind level------------------------------------------------------------*/
            custom.data.child.push(FormlyFactory.TableControl)
            /*Bind level------------------------------------------------------------*/

            control.data.child.push(input);
            control.data.child.push(textArea);
            control.data.child.push(dropdown);
            control.data.child.push(checkbox);
            control.data.child.push(upload);
            control.data.child.push(custom);
            /*Bind level------------------------------------------------------------*/
            $scope.component.push(label);
            $scope.component.push(control);

            /*END declare variable..........................................................*/

            /*START do amination..........................................................*/
            $timeout(function () {
                $("#formly-toolbox").slimScroll({
                    height: "80vh",
                });
                $("#formly-mainscreen").slimScroll({
                    height: "80vh",
                });
            });

            $scope.ClickToCategory = function (item) {
                selectingItem.data.Selected = false;
                selectingItem = item;
                selectingItem.data.Selected = true;
                selectingItem.data.IsExpand = !selectingItem.data.IsExpand;
            };
            var initSortableAndDropable = function () {
                $timeout(function () {
                    var startposRow;
                    var endposRow;
                    try {
                        $(".sortablerow").sortable("destroy");
                    } catch (e) {
                        // console.log(e);
                    }
                    $(".sortablerow").sortable({
                        placeholder: "ui-state-highlight",
                        tolerance: "pointer",
                        forcePlaceholderSize: true,
                        stop: function (event, ui) {
                            endposRow = ui.item.index();
                            var b = $scope.vm.Model[endposRow];
                            $scope.vm.Model[endposRow] = $scope.vm.Model[startposRow]
                            $scope.vm.Model[startposRow] = b;
                        },
                        start: function (event, ui) {
                            startposRow = ui.item.index();
                        },
                    });
                    $(".sortablerow").disableSelection();
                    var currentSortListId;
                    var startpos;
                    var endpos;
                    try {
                        $(".sortable").sortable("destroy");
                    } catch (e) {
                        // console.log(e);
                    }
                    $(".sortable").sortable({
                        placeholder: "ui-state-highlight",
                        tolerance: "pointer",
                        forcePlaceholderSize: true,
                        stop: function (event, ui) {
                            currentSortListId = ui.item.parent().attr("id");
                            endpos = ui.item.index();
                            angular.forEach($scope.vm.Model, function (component) {
                                if (component.data.index == currentSortListId) {
                                    var b = component.fieldGroup[endpos];
                                    component.fieldGroup[endpos] = component.fieldGroup[startpos];
                                    component.fieldGroup[startpos] = b;
                                }
                            });
                        },
                        start: function (event, ui) {
                            startpos = ui.item.index();
                        },
                    });
                    $(".sortable").disableSelection();
                    try {
                        $(".droppable").droppable("destroy");
                    } catch (e) {
                        //console.log(e);
                    }
                    $(".droppable").droppable({
                        accept: ".draggable",
                        tolerance: "pointer",
                        over: function (event, ui) {
                            $(this).addClass("hover");
                        },
                        out: function (event, ui) {
                            $(this).removeClass("hover");
                        },
                        drop: function (event, ui) {
                            $(this).removeClass("hover");
                            var index = $(this).attr("id");

                            var putitem = angular.copy(selectingItem);
                            putitem.key = FormlyService.NewGuid();
                            angular.forEach($scope.vm.Model, function (row) {
                                if (row.data.index == index) {

                                    row.fieldGroup.push(putitem);
                                }
                            });

                            $scope.$apply();
                        }
                    });
                });
            };
            var reloadJqueryObj = function () {
                var item = angular.copy($scope.vm.Model);
                delete $scope.vm.Model;
                $scope.vm.Model = item;
                initSortableAndDropable();
            };
            var getAllKey = function () {
                var listkey = [];
                //2 lớp only
                for (var i = 0; i < $scope.vm.Model.length; i++) {
                    var row = $scope.vm.Model[i];
                    for (var j = 0; j < row.fieldGroup.length; j++) {
                        var compoment = row.fieldGroup[j];
                        listkey.push(compoment.key);
                    }
                }
                return listkey;
            }

            var initDraggable = function () {
                $timeout(function () {
                    try {
                        $(".draggable").draggable("destroy");
                    } catch (e) {
                        // console.log(e);
                    }
                    $(".draggable").draggable({
                        revert: "invalid", // when not dropped, the item will revert back to its initial position
                        appendTo: "body",
                        cursorAt: {
                            top: 0,
                            left: 0
                        },
                        cursor: "-webkit-grabbing",
                        //delay: 300,
                        distance: 5, //khoang cach pixel chuot bat buoc phai di truoc khi muon dragg
                        //refreshPositions: true,
                        helper: function () {
                            return '<span class="alert alert-success" > <i class="fa fa-lg fa-plus-circle"></i> </span>'
                            // var item = $(this).clone();
                            // return item;
                        }
                    });
                });
            };
            //Thêm một dòng
            $scope.AddRow = function () {
                var fieldGroup = [];
                //fieldGroup.push(FormlyFactory.InputTextControl);
                //fieldGroup.push(FormlyFactory.InputEmailControl);
                //fieldGroup.push(FormlyFactory.InputUrlControl);
                //fieldGroup.push(FormlyFactory.InputNumberControl);
                //fieldGroup.push(FormlyFactory.InputRangeControl);
                //fieldGroup.push(FormlyFactory.InputDateControl);
                //fieldGroup.push(FormlyFactory.InputPasswordControl);
                //fieldGroup.push(textAreaControl);
                //fieldGroup.push(FormlyFactory.InputColorControl);
                $scope.vm.Model.push({
                    "data": {
                        "type": 1,
                        "index": FormlyService.NewGuid(),
                        "name": "Row"
                    },
                    "wrapper": "layout",
                    "className": "row",
                    "fieldGroup": fieldGroup,
                });
                var item = angular.copy($scope.vm.Model);
                delete $scope.vm.Model;
                $scope.vm.Model = item;
                initSortableAndDropable();
            }
            //Thêm một dòng
            $scope.AddTable = function () {
                var fieldGroup = [];
                $scope.vm.Model.push({
                    "data": {
                        "type": 1,
                        "index": FormlyService.NewGuid(),
                        "name": "Bảng"
                    },
                    "wrapper": "table",
                    "className": "row",
                    "fieldGroup": fieldGroup,
                });
                var item = angular.copy($scope.vm.Model);
                delete $scope.vm.Model;
                $scope.vm.Model = item;
                initSortableAndDropable();
            }

            //Sửa một row
            $scope.EditRow = function (compoment) {
                var modalInstance;
                modalInstance = $uibModal.open({
                    templateUrl: template.editRowUrl,
                    controller: controller.editRowCtr,
                    size: 'lg',
                    // windowClass :"modal-full",
                    backdrop: 'static',
                    // Set parameter to chidform (popup form)
                    resolve: {
                        item: function () {
                            return compoment;
                        },

                    }
                });
                modalInstance.result.then(function (response) {
                    //2 lớp only
                    var index = $scope.vm.Model.indexOf(compoment);
                    if (index >= 0) {
                        $scope.vm.Model[index] = response;

                        reloadJqueryObj();
                    }
                }, function (response) {});
            };
            //Xóa một dòng
            $scope.DeleteRow = function (compoment) {
                var index = $scope.vm.Model.indexOf(compoment);
                if (index >= 0) {
                    $scope.vm.Model.splice(index, 1);
                }
                var item = angular.copy($scope.vm.Model);
                delete $scope.vm.Model;
                $scope.vm.Model = item;
                initSortableAndDropable();
            }
            //sao chép dòng một dòng
            $scope.CloneRow = function (compoment) {
                var index = $scope.vm.Model.indexOf(compoment);
                if (index >= 0) {
                    var clone = angular.copy(compoment);
                    clone.data.index = FormlyService.NewGuid();
                    $scope.vm.Model.splice(index, 0, clone);
                }
                reloadJqueryObj();
            }
            $scope.RowSelecting = null;
            //Đổi vị trí dòng
            $scope.CheckSwapRow = function (compoment) {
                if (compoment === $scope.RowSelecting) {
                    return "btn-danger";
                } else {
                    if ($scope.RowSelecting === null) {
                        return "";
                    } else {
                        return "btn-primary";
                    }
                }
            };
            $scope.SwapRow = function (compoment) {

                if ($scope.RowSelecting === null) {
                    $scope.RowSelecting = compoment;

                } else {
                    if (compoment === $scope.RowSelecting) {
                        $scope.RowSelecting = null;
                    } else {
                        var indexA = $scope.vm.Model.indexOf(compoment);
                        var indexB = $scope.vm.Model.indexOf($scope.RowSelecting);
                        var temp = $scope.vm.Model[indexA];
                        $scope.vm.Model[indexA] = $scope.vm.Model[indexB];
                        $scope.vm.Model[indexB] = temp;
                        $scope.RowSelecting = null;
                        var item = angular.copy($scope.vm.Model);
                        delete $scope.vm.Model;
                        $scope.vm.Model = item;
                    }
                }

            };
            $scope.OldColumnName = "";
            $scope.OldDbType = "";

            //Sửa một item
            $scope.EditItem = function (compoment, type) {

                var modalInstance;
                var listkey = getAllKey();
                if (type == "label") {
                    modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: template.editLabelUrl,
                        controller: controller.editLabelCtr,
                        size: 'lg',
                        // windowClass :"modal-full",
                        backdrop: 'static',
                        // Set parameter to chidform (popup form)
                        resolve: {
                            item: function () {
                                return compoment
                            },

                        }
                    });
                } else {
                    modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: template.editControlUrl,
                        controller: controller.editControlCtr,
                        size: 'lg',
                        // windowClass :"modal-full",
                        backdrop: 'static',
                        // Set parameter to chidform (popup form)
                        resolve: {
                            item: function () {
                                return compoment
                            },
                            option: function () {
                                var obj = {};
                                obj.listkey = listkey;
                                return obj
                            },
                        }
                    });
                }

                modalInstance.result.then(function (response) {
                    //2 lớp only
                    angular.forEach($scope.vm.Model, function (row) {
                        var index = row.fieldGroup.indexOf(compoment);
                        if (index >= 0) {
                            row.fieldGroup[index] = response;

                            reloadJqueryObj();
                        }
                    });
                }, function (response) {});
            };
            //Clone một item
            $scope.CloneItem = function (compoment) {
                //2 lớp only
                angular.forEach($scope.vm.Model, function (row) {
                    var index = row.fieldGroup.indexOf(compoment);
                    if (index >= 0) {

                        var clone = angular.copy(compoment);
                        clone.key = FormlyService.NewGuid();
                        row.fieldGroup.splice(index, 0, clone);


                        reloadJqueryObj();


                    }
                });
            };
            //Xóa một item
            $scope.DeleteItem = function (compoment) {
                //2 lớp only
                angular.forEach($scope.vm.Model, function (row) {
                    var index = row.fieldGroup.indexOf(compoment);
                    if (index >= 0) {
                        row.fieldGroup.splice(index, 1);


                        reloadJqueryObj();
                    }
                });
            };
            $scope.$watch("filterText", function () {
                initDraggable();
            });

            //Chuyển form config <=> preview
            $scope.ChangeForm = function (type) {
                $scope.vm.Scheme = [];
                $scope.vm.Data = {};
                $scope.FormMode = type;
                if ($scope.FormMode == "preview") {

                    $scope.vm.Scheme = angular.copy($scope.vm.Model);
                }
            }

            //Hành động commitform
            $scope.vm.onSubmit = onSubmit;

            function onSubmit() {
                // console.log('form submitted:', $scope.vm.user);
            }

            initDraggable();

            $scope.getNumber = function (num) {
                return new Array(num);
            }

            $scope.range = function (min, max, step) {
                step = step || 1;
                var input = [];
                for (var i = min; i <= max; i += step) {
                    input.push(FormlyFactory.I);
                }
                return input;
            };

            $scope.getIndexFielGroup = function (row, col, numberCol) {
                return (row + 1) * numberCol + col;
            }

            //function for check form valid
            var checkFormValid = function () {
                if ($scope.FormName == null ||
                    $scope.FormName.trim() == "" ||
                    $scope.FormName.length > 255) {
                    $scope.FormValid.FormName = false;
                }

                if ($scope.FormCode == null ||
                    $scope.FormCode.trim() == "" ||
                    $scope.FormCode.length > 16) {
                    $scope.FormValid.FormCode = false;
                }


                if ($scope.StartDate == null) {
                    $scope.FormValid.StartDate = false;
                }
                //if ($scope.EndDate == null) {
                //    $scope.FormValid.EndDate = false;
                //}
                return ($scope.FormValid.FormCode || $scope.FormValid.FormName || $scope.FormValid.EndDate || $scope.FormValid.StartDate);
            }
        }
    ]);
});