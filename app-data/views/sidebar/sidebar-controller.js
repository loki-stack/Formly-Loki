﻿// 'use strict';
/* SAVIS Vietnam Corporation
 *
 * This module is to separate configuration of authentication to app.js
 * NguyenBv - Jan 2017
 */

define(['jquery', 'angularAMD', 'components/service/amdservice', 'Metronic.Layout'], function (jQuery, angularAMD, authConfig) {
    'use strict';
    /* Init the auth config, interceptor, and runtime modules */
    var factory = {};


    factory.init = function (app) {
        app.controller('SideBarCtrl', ['$scope', '$q', '$timeout', '$rootScope', '$routeParams', '$location', '$route', '$http', 'constantsAMD', '$log', '$translate',
            function ($scope, $q, $timeout, $rootScope, $routeParams, $location, $route, $http, constantsAMD, $log, $translate) {
                Layout.initSidebar(); // init sidebar
            }
        ]);
    };

    return factory;
});