'use strict';
define(['app', 'jwplayer'], function(app) {
    app.directive('jwplayerDirective', [function() {
        return function (scope, elm, attrs) {

            jwplayer(attrs.id).setup({
		        file: attrs.video,
		        width: attrs.width,
		        height: attrs.height,
		        primary: attrs.primary,
		        type: attrs.type,
		        //autostart: true
        	});        	

            scope.$watch(attrs.video, function (newValue, oldValue) {
                jwplayer().remove();
		         jwplayer(attrs.id).setup({
		             video: newValue,
		         });
		     });
		     scope.$watch(attrs.primary, function (newValue, oldValue) {
		         jwplayer().remove();
		         jwplayer(attrs.id).setup({
		             primary: newValue,
		         });
		     });
		     scope.$watch(attrs.type, function (newValue, oldValue) {
		         jwplayer().remove();
		         jwplayer(attrs.id).setup({
		             type: newValue,
		         });
		     });
		    
        };
    }]);
})
