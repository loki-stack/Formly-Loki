﻿
/* SAVIS Vietnam Corporation
*
* This module is to separate configuration of routing to app.js
* TruongND - May 2015
*/
define(['components/service/amdservice',], function () {
    'use strict';
    var basedUrl = "/app-data/";

    var factory = {};

    /* Setup all routings */
    factory.setupRouteConfig = function (app, angularAMD) {
        // Route config
        app.config(['$routeProvider', function ($routeProvider) {
            // User profile
            $routeProvider
                .when("/formly", angularAMD.route({
                    templateUrl: basedUrl + 'views/formly/formly.html',
                    controller: 'FormlyCtrl',
                    controllerUrl: 'views/formly/formly',
                    resolve: {
                        Title: function ($rootScope, $route) {
                            $rootScope.pageTitle = 'Formy';
                            return
                        }
                    },
                }))
                .otherwise({
                    redirectTo: '/formly',
                });
        }]);
    };
    return factory;
});
