﻿'use strict';
define(['app', 'components/service/amdservice', 'components/factory/factory'], function (app) {

    /*
        START
        ARM.V2 API SERVICE, 
        SONPN 
    */
    app.service('MDMApiService', ['$http', 'Base64', 'constantsFactory', function ($http, Base64, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrlMDM;
        var prefixCoreApiUrl = "api/v1/core/nodes";
        service.apiUrl = prefixCoreApiUrl;
        var authUserName = 'ECM_AUTH_API';
        var authPassword = 'secret';
        var authStr = Base64.encode(authUserName + ':' + authPassword);
        //Cấu trúc thư mục
        //ARM-Temp/<năm>/<module>/<subInfo>/<tháng>/<ngày>
        //ARM/<năm>/<module>/<id module>/<subInfo>/
        service.GetUploadSrc = function (moduleName, subInfo) {
            //fix const 
            var appId = '48ed5b71-66dc-4725-9604-4c042e45fa3f';
            var createdByUserName = 'savis_admin';
            var userId = '7efa183f-0013-43d6-a972-29abd7a93bc1';
            var isScanvirus = false;
            var today = new Date();
            var destinationPath;
            if (subInfo == null) {
                destinationPath = "ARM-Temp/" + today.getFullYear() + "/" + moduleName + "/" + (today.getMonth() + 1) + "/" + today.getDate();
            } else {
                destinationPath = "ARM-Temp/" + today.getFullYear() + "/" + moduleName + "/" + subInfo + "/" + (today.getMonth() + 1) + "/" + today.getDate();
            }
            console.log("destinationPath", destinationPath);


            var result = baseUrl + prefixCoreApiUrl + "/upload" +
                '?AppId=' + appId +
                '&UserId=' + userId +
                '&CreateByUser=' + createdByUserName +
                '&DestinationPath=' + destinationPath +
                //'&DestinationPhysicalPath=' + destinationPhysicalPath +
                '&IsScanvirus=' + isScanvirus +
                '&[[Authencation:' + authStr + ']]'

            return result;
        };

        service.GetThumbnailSrc = function (nodeId, width, height, type, quality) {
            var result = baseUrl + prefixCoreApiUrl + "/" + nodeId + "/thumbnail/view?w=" + width + "&h=" + height + "&t=" + type;
            if (typeof (quality) !== "undefined")
                result = result + "&1=" + quality
            //+ '&[[Authencation:' + authStr + ']]';
            return result;
        };

        service.GetViewFileSrc = function (nodeId) {
            var result = baseUrl + prefixCoreApiUrl + "/" + nodeId + "/view"
            //+ '&[[Authencation:' + authStr + ']]';
            return result;
        };
        service.GetMdmDownloadLink = function (nodeId) {
            var result = baseUrl + prefixCoreApiUrl + "/" + nodeId + "/download"
            //+ '&[[Authencation:' + authStr + ']]';
            return result;
        };
        service.GetFileAttachment = function (nodeId) {//get file and review by noteId
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + nodeId + "/view",
                responseType: 'arraybuffer'
            });
            return promise;
        };

        return service;

    }]);
    app.service('LogService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/system/log";
        service.apiUrl = prefixCoreApiUrl;
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        return service;

    }]);
    app.service('ArmV2ApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var basedUrl = constantsFactory.ApiUrl;

        // ---------------------------------------- Comment api service

        service.GetCommentList = function (data) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + constantsFactory.ApiCommentUrl + '/query',
                headers: {
                    'Content-type': ' application/json'
                },
                data: data
            });
            return promise;
        };
        service.GetMdmDownloadLink = function (nodeId) {
            return constantsFactory.ApiUrlMDM + "api/v1/core/nodes/" + nodeId + "/download";
        };

        service.AddCommentList = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + constantsFactory.ApiCommentUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: JSON.stringify(postData)
            });
            return promise;
        };

        return service;

    }]);
    app.service('DeliveryRecordsApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var baseUrl = constantsFactory.ApiUrl;

        var prefixCoreApiUrl = "api/v2/arm/deliveryrecords";

        service.GetById = function (deliveryrecordsId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '/' + deliveryrecordsId,

            });
            return promise;
        };
        service.GetFilter = function (requestData) {
            var filter = angular.toJson(requestData);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '?filter=' + filter,
            });
            return promise;
        };
        service.Create = function (requestData) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                },
            });
            return promise;
        };
        service.Update = function (requestData) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl + "/update",
                data: JSON.stringify(requestData),
                headers: {
                    'Content-Type': 'application/json'
                },
            });
            return promise;
        };
        service.UpdateStatus = function (updateData) {
            var m = JSON.stringify(updateData);
            var promise = $http({
                method: 'PATCH',
                url: baseUrl + prefixCoreApiUrl + "/" + updateData.Id + "/updateStatus",
                data: updateData,
                headers: {
                    'Content-Type': 'application/json'
                },
            });
            return promise;
        };
        service.Delete = function (DeliveryRecordsId) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + DeliveryRecordsId

            });
            return promise;
        };
        service.DeleteMany = function (listDeleteId) {

            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl,
                data: listDeleteId,
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            return promise;
        };
        service.GetFilterFondProfile = function (requestData) {
            var filter = angular.toJson(requestData);
            var promise = $http({
                method: 'GET',
                url: baseUrl + 'api/v2/arm/deliveryrecordFondProfile?filter=' + filter,
            });
            return promise;
        };
        return service;

    }]);
    app.service('CartsApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var baseUrl = constantsFactory.ApiUrl;

        var prefixCoreApiUrl = "api/v2/arm/carts";

        service.GetById = function (deliveryrecordsId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '/' + deliveryrecordsId,

            });
            return promise;
        };
        service.GetFilter = function (requestData) {
            var filter = angular.toJson(requestData);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + 'Old?filter=' + filter,
            });
            return promise;
        };
        service.GetRecordProductsFilter = function (requestData) {
            var filter = angular.toJson(requestData);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '?filter=' + filter,
            });
            return promise;
        };
        service.Create = function (requestData) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                },
            });
            return promise;
        };
        service.Update = function (requestData) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl + "/update",
                data: JSON.stringify(requestData),
                headers: {
                    'Content-Type': 'application/json'
                },
            });
            return promise;
        };
        service.Delete = function (DeliveryRecordsId) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + DeliveryRecordsId

            });
            return promise;
        };
        service.DeleteMany = function (listDeleteId) {

            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl,
                data: listDeleteId,
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            return promise;
        };
        service.UpdateMany = function (requestData) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/updatemany",
                data: JSON.stringify(requestData),
                headers: {
                    'Content-Type': 'application/json'
                },
            });
            return promise;
        };
        return service;

    }]);
    app.service('WorkflowApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/workflow/documents";
        var prefixExtentApiUrl = "api/v2/arm/fond-profile/workflow";

        service.GetDocumentById = function (documentId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '/' + documentId,

            });
            return promise;
        };

        service.GetDocumentHistory = function (documentId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '/' + documentId + "/transitionhistory",
            });
            return promise;
        };

        service.ProcessRecord = function (documentId, requestData) {

            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixExtentApiUrl + '/' + documentId + "/process",
                data: requestData
            });
            return promise;
        };

        service.ProcessDocument = function (documentId, requestData) {
            /*  public class WorkflowProcessDocumentModel
                {
                    public string DocumentId { get; set; }
                    public string DocumentType { get; set; }
                    public string Command { get; set; }
                    public string Comment { get; set; }
                    public string UserId { get; set; }
                    public string ActorId { get; set; }
                }  */
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl + '/' + documentId + "/process",
                data: requestData
            });
            return promise;
        };

        service.GetAvailableProcessCommands = function (documentId, userId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '/' + documentId + "/availablecommands?userId=" + userId,
            });
            return promise;
        };

        service.GetSchemeByUser = function (userId) {
            var workflowData = [];

            var promiseCurrentWorkflow =
                $http({
                    method: 'GET',
                    url: baseUrl + constantsFactory.ApiUsersUrl + '/' + userId + '/scheme'
                });

            return promiseCurrentWorkflow;
        };

        return service;

    }]);
    app.service('RecordCollectionApiService', ['$http', 'constantsFactory', 'constantsAMD', function ($http, constantsFactory, constantsAMD) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/v2/arm/record-collection";

        service.GetById = function (recordCollectionId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '/' + recordCollectionId
            });
            return promise;
        };
        service.GetByFondProfileId = function (fondProfileId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '/fond-profile/' + fondProfileId
            });
            return promise;
        };
        service.GetFilter = function (requestData) {
            var filter = angular.toJson(requestData);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '?stringFilter=' + filter
            });
            return promise;
        };
        service.GetByApplication = function (application, fondId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '/get-by-app/' + application + '/' + fondId
            });
            return promise;
        };

        service.Create = function (requestData) {
            // requestData.WorkflowCode = "ARM_2STEP";
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: requestData
            });
            return promise;
        };
        service.Update = function (requestData) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + requestData.RecordCollectionId,
                data: requestData

            });
            return promise;
        };
        service.DeleteById = function (recordCollectionId) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + recordCollectionId

            });
            return promise;
        };
        service.DeleteMany = function (listRecordCollectionId) {
            var requestData = {};
            requestData.ListRecordCollectionId = listRecordCollectionId;
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl,
                data: requestData,
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            return promise;
        };
        service.GetAvailableCommands = function (recordCollectionId, userid) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + recordCollectionId + "/workflow/availablecommands?userId=" + userid
            });
            return promise;
        };
        service.checkDuplicateByFondId = function (fondId, recordCollectionNumber, recordCollectionId) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + '/check-duplicate-number/' + fondId + '/' + recordCollectionId + '/' + recordCollectionNumber
            });
            return promise;
        };
        service.ProcessDocument = function (recordCollectionId, command, comment, userid, actorId) {
            var requestData = {
                "DocumentId": recordCollectionId,
                "DocumentType": "",
                "Command": command,
                "Comment": comment,
                "UserId": userid,
                "ActorId": userid,
            };

            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl + "/" + recordCollectionId + "/workflow/process",
                data: requestData,
            });
            return promise;
        };
        ///////////////////////////////////////////////////////////////
        service.GetDecisionByRecordCollectionId = function (recordCollectionId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + recordCollectionId + "/decisions",
            });
            return promise;
        };
        service.UpdateDecision = function (recordCollectionId, decisionUserId, decisionSign, decisionDate, type, recordCollectionDecisionId) {

            var requyestData = {
                RecordCollectionDecisionId: recordCollectionDecisionId,
                RecordCollectionId: recordCollectionId,
                DecisionByUserId: decisionUserId,
                DecisionSign: decisionSign,
                DecisionDate: decisionDate
            };
            var promise;
            if (type === "add") {
                promise = $http({
                    method: 'POST',
                    url: baseUrl + prefixCoreApiUrl + "/" + recordCollectionId + "/decisions",
                    data: requyestData
                });
            } else {
                promise = $http({
                    method: 'PUT',
                    url: baseUrl + prefixCoreApiUrl + "/" + recordCollectionId + "/decisions/" + recordCollectionDecisionId,
                    data: requyestData
                });
            }
            return promise;
        };

        service.GenarateTemplateDecision = function (recordCollectionId) {
            constantsAMD.saveToDiskFromApi(baseUrl + prefixCoreApiUrl + "/" + recordCollectionId + "/decisions/genarate", "Template.xml");
        }

        //service.GetUploadSrc = function () {
        //    //fix
        //    var uploadUrl = constantsFactory.ApiUrlMDM;
        //    var parentNodeId = ;
        //    var createdByUserName =;
        //    var appId =;
        //    var metadataTemplateId =;
        //    var isScanvirus =;




        //    var result = uploadUrl + "api/v1/core/nodes/upload" + '?CreateByUser=' + createdByUserName +
        //        '&AppId=' + appId +
        //        '&MetadataTemplateId=' + metadataTemplateId +
        //        '&IsScanvirus=' + isScanvirus
        //    return result;
        //}

        //hoant: api/v2/arm/record-collection/{recordCollectionId}/{checkAll}

        service.UpdateReception = function (id, ischeckall, updateModel) {
            console.log(updateModel)
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/" + ischeckall,
                data: updateModel
            });
            return promise;
        };
        return service;

    }]);
    app.service('FondApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var baseUrl = constantsFactory.ApiUrl;

        var prefixCoreApiUrl = "api/v2/arm/fond";

        service.GetById = function (fondId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '/' + fondId,

            });
            return promise;
        };

        service.GetByArchiveTypeId = function (archiveTypeId, applicationId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '/?archiveTypeId=' + archiveTypeId + '&applicationId=' + applicationId,

            });
            return promise;
        };
        service.GetFilter = function (requestData) {
            var filter = angular.toJson(requestData);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + 's' + '?filter=' + filter,
            });
            return promise;
        };
        service.Create = function (requestData) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: requestData,

            });
            return promise;
        };
        service.Update = function (requestData) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + requestData.FondId,
                data: requestData,

            });
            return promise;
        };
        service.DeleteById = function (FondId) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + FondId

            });
            return promise;
        };
        service.DeleteMany = function (listDeleteId) {

            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl,
                data: listDeleteId,
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            return promise;
        };
        return service;

    }]);
    app.service('CouncilApprovalApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var baseUrl = constantsFactory.ApiUrl;

        var prefixCoreApiUrl = "api/v2/arm/councils";

        service.GetFilter = function (filter) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '?filter=' + filter,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        service.GetMemberOfCouncilApprovalById = function (councilId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '/members/' + councilId,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        service.GetByOrgId = function (organizationId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '/' + organizationId + '/organizations',
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        service.GetChucDanhs = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + "api/catalogs/catalogmaster/chuc-vu-hdtd/itemlist",
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        service.Update = function (model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + '/' + model.CouncilApprovalId,
                data: model
            });
            return promise;
        }

        service.Add = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: model
            });
            return promise;
        }

        service.DeleteMutil = function (listId) {
            var mutilDeleteModel = {};
            mutilDeleteModel.ListCouncilApprovalId = listId;

            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl,
                headers: {
                    'Content-type': ' application/json'
                },
                data: mutilDeleteModel
            });
            return promise;
        }

        return service;

    }]);
    app.service('FondProfileApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var baseUrl = constantsFactory.ApiUrl;

        var prefixCoreApiUrl = "api/v2/arm/fond-profile";

        service.GetById = function (fondProfileId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '/' + fondProfileId,

            });
            return promise;
        };
        service.GetFilter = function (requestData) {
            var filter = angular.toJson(requestData);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '/query?filter=' + filter,
            });
            return promise;
        };
        service.Create = function (requestData) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: requestData,

            });
            return promise;
        };
        service.Createhsp = function (requestData) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl + "/ho-so-phong",
                data: requestData,

            });
            return promise;
        };
        service.Update = function (requestData) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + requestData.FondProfileId,
                data: requestData,

            });
            return promise;
        };
        service.DeleteById = function (fondProfileId) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + fondProfileId

            });
            return promise;
        };
        service.CheckRoleDelete = function (fondProfileId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/check-role-delete/" + fondProfileId

            });
            return promise;
        };

        return service;

    }]);
    app.service('RecordDocumentApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/v1/arm/documents";


        service.GetById = function (recordDocumentId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '/' + recordDocumentId,

            });
            return promise;
        };
        service.Create = function (requestData) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + 'api/v1/arm/records/' + requestData.RecordId + '/documents',
                data: requestData
            });
            return promise;
        };
        service.Update = function (requestData) {
            //console.log('PUT', requestData);
            var promise = $http({
                method: 'PUT',
                url: baseUrl + 'api/v1/arm/records/' + requestData.RecordId + '/documents/' + requestData.RecordDocumentId,
                data: requestData

            });
            return promise;
        };
        service.DeleteById = function (recordDocumentId) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + recordDocumentId,
            });
            return promise;
        };


        return service;

    }]);
    app.service('PhongBanApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;

        service.GetAllPhongBan = function (curentPage, pageSize, textSearch) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + constantsFactory.ApiPhongBanUrl + '?pageNumber=' + curentPage + '&pageSize=' + pageSize + '&textSearch=' + textSearch
            });
            return promise;
        };

        service.AddPhongBan = function (pData) {

            var promise = $http({
                method: 'POST',
                url: basedUrl + constantsFactory.ApiPhongBanUrl,
                //headers: {
                //    'Content-type': ' application/json'
                //},
                data: pData
            });
            return promise;
        };

        service.EditPhongBan = function (pData) {

            var promise = $http({
                method: 'PUT',
                url: basedUrl + constantsFactory.ApiPhongBanUrl + '/' + pData.OrganizationId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: pData
            });
            return promise;
        };

        service.DeletePhongBan = function (organizationId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + constantsFactory.ApiPhongBanUrl + '/' + organizationId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            service.DeleteMany = function (model) {
                var promise = $http({
                    method: 'DELETE',
                    url: baseUrl + constantsFactory.ApiPhongBanUrl,
                    data: model
                });
                return promise;
            }

            return promise;
        };

        service.GetAllOrganizations = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + constantsFactory.ApiPhongBanUrl
            });
            return promise;
        };
        service.GetAllOrganizationsByParentId = function (id) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + constantsFactory.ApiPhongBanUrl + '/Parent/' + id
            });
            return promise;
        };
        //api / organizations / Parent
        service.GetOrgnazitionByUserId = function (userId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + "api/v2/admin/organizations/" + userId + "/users"
            });
            return promise;
        };
        //api/organizations/listtree
        service.GetOrgnazitionListTree = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + constantsFactory.ApiPhongBanUrl + "/listtree"
            });
            return promise;
        };
        //api/organizations
        service.GetOrgnazitionFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: basedUrl + constantsFactory.ApiPhongBanUrl + "/filter?stringFilter=" + stringFilter
            });
            return promise;
        };
        // Return service
        return service;

    }]);
    app.service('TaxonomyApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;

        service.GetAllTaxonomy = function (vocabulary, type) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + 'api/taxonomy/' + vocabulary + '/terms?typeOfDisplay=' + type,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };
        service.CreateTaxonomy = function (vocabularyId, vocabularyName, name, parentId) {
            var requestData = {};
            requestData.Name = name;
            requestData.VocabularyId = vocabularyId;
            requestData.VocabularyName = vocabularyName;
            if (typeof parentId !== 'undefined') {
                requestData.ParentTermId = parentId;
            }
            requestData.CreatedByUserId = "48ed5b71-66dc-4725-9604-4c042e45fa3f";
            requestData.ModifiedByUserId = "48ed5b71-66dc-4725-9604-4c042e45fa3f";
            requestData.ApplicationID = "48ed5b71-66dc-4725-9604-4c042e45fa3f";

            var promise = $http({
                method: 'POST',
                url: basedUrl + 'api/taxonomy/term',
                headers: {
                    'Content-type': ' application/json'
                },
                data: requestData
            });
            return promise;
        };


        service.UpdateTaxonomy = function (vocabularyId, vocabularyName, name, parentId, termId) {
            var requestData = {};
            requestData.Name = name;
            requestData.TermId = termId;
            requestData.VocabularyId = vocabularyId;
            requestData.VocabularyName = vocabularyName;
            if (typeof parentId !== 'undefined') {
                requestData.ParentTermId = parentId;
            }
            requestData.CreatedByUserId = "48ed5b71-66dc-4725-9604-4c042e45fa3f";
            requestData.ModifiedByUserId = "48ed5b71-66dc-4725-9604-4c042e45fa3f";
            requestData.ApplicationID = "48ed5b71-66dc-4725-9604-4c042e45fa3f";

            var promise = $http({
                method: 'PUT',
                url: basedUrl + 'api/taxonomy/term/' + termId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: requestData
            });
            return promise;
        };





        service.DeleteTaxonomy = function (termId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + "api/taxonomy/delete/" + termId,
                headers: {
                    'Content-type': ' application/json'
                }
            });
            return promise;
        };


        // Return service
        return service;

    }]);
    app.service('RightsApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;

        service.GetRightsByUserId = function (userId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + 'api/system/rights/' + '?userId=' + userId,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        // Return service
        return service;

    }]);
    app.service('ParametersApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;

        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + "api/parameter"
            });
            return promise;
        };

        service.AddParameter = function (requestData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "api/parameter",
                data: requestData
            });
            return promise;
        };


        service.UpdateParameter = function (requestData) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + "api/parameter/" + requestData.ParameterID,
                data: requestData
            });
            return promise;
        };


        service.DeleteParameter = function (parameterID) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + "api/parameter/" + parameterID,
            });
            return promise;
        };

        service.DeleteManyParameter = function (requestData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "api/parameter/deletes",
                data: requestData
            });
            return promise;
        };
        // Return service
        return service;

    }]);
    app.service('UserApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;

        service.GetUserById = function (id) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + constantsFactory.ApiUsersUrl + "/" + id
            });
            return promise;
        };

        service.GetAllUserActive = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + constantsFactory.ApiUsersUrl + "/active"
            });
            return promise;
        };
        service.GetAllUserByOrg = function (orgId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + constantsFactory.ApiUsersUrl + "/" + orgId + "/organization"
            });
            return promise;
        };
        service.GetUserByOrgInTreeView = function (orgId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + constantsFactory.ApiUsersUrl + "/" + orgId + "/organization/treeview"
            });
            return promise;
        };

        service.GetAllUserInOrg = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + constantsFactory.ApiUsersUrl + "/organization"
            });
            return promise;
        };

        service.GetFilter = function (model) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + constantsFactory.ApiUsersUrl + "/query",
                data: model
            });
            return promise;
        };
        // Return service
        return service;

    }]);
    app.service('FieldService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/field";

        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        }
        return service;

    }]);
    app.service('ArchiveTypeService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/archivetype";

        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        }

        service.Clone = function (id) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/clone"
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        }

        service.GetFieldByArchiveTypeId = function (id, type, typeDisplay) {
            if (id === "all") {
                id = "00000000-0000-0000-0000-000000000000";
            }
            var _url = baseUrl + prefixCoreApiUrl + "/" + id + "/field/" + type + '/' + typeDisplay;
            var promise = $http({
                method: 'GET',
                url: _url
            });
            return promise;
        }
        return service;

    }]);
    app.service('RecordService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/record";

        service.GetRecordByToken = function (token, readerId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/token?readerId=" + readerId + "&token=" + token
            });
            return promise;
        };
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetByRecordCollectionId = function (recordCollectionId, isGetData) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/record-collection/" + recordCollectionId + "/" + isGetData
            });
            return promise;
        }

        service.GetFilterResearch = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "s?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        }

        service.GetFieldByRecordId = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/field"
            });
            return promise;
        }

        service.GetAtributeById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/data/"
            });
            return promise;
        }

        service.GetAtributeResearchById = function (recordId, archiveTypeId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + recordId + "/data?archiveTypeId=" + archiveTypeId
            });
            return promise;
        }

        return service;

    }]);
    app.service('ImportRecordService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/record-import";

        service.ImportToArm = function (fondId, recordCollectionId, isImportAll) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/import-arm/" + fondId + "/" + recordCollectionId + "/" + isImportAll
            });
            return promise;
        }
        service.CheckData = function (id, value) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/check-data/" + id + "/" + value
            });
            return promise;
        }
        service.CheckDataAll = function (listRecordId, value) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/check-data-all/" + value,
                data: listRecordId
            });
            return promise;
        }
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetByRecordCollectionId = function (recordCollectionId, isGetData) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/record-collection/" + recordCollectionId + "/" + isGetData
            });
            return promise;
        }

        service.GetFilterResearch = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "s?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        }

        service.GetFieldByRecordId = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/field"
            });
            return promise;
        }

        service.GetAtributeById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/data/"
            });
            return promise;
        }
        return service;

    }]);
    app.service('DocumentService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/document";
        service.GetDocumentByToken = function (token, readerId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/token?readerId=" + readerId + "&token=" + token
            });
            return promise;
        };
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetByDocumentCollectionId = function (documentCollectionId, isGetData) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/document-collection/" + documentCollectionId + "/" + isGetData
            });
            return promise;
        }

        service.GetFilterResearch = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "s?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        }

        service.GetFieldByDocumentId = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/field"
            });
            return promise;
        }

        service.GetAtributeById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/data/"
            });
            return promise;
        }
        return service;

    }]);
    app.service('ImportDocumentService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/document-import";
        service.ImportToArm = function (fondId, recordCollectionId, recordId, isImportAll) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/import-arm/" + fondId + "/" + recordCollectionId + "/" + recordId + "/" + isImportAll
            });
            return promise;
        }
        service.CheckData = function (id, value) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/check-data/" + id + "/" + value
            });
            return promise;
        }
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetByDocumentCollectionId = function (documentCollectionId, isGetData) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/document-collection/" + documentCollectionId + "/" + isGetData
            });
            return promise;
        }

        service.GetFilterResearch = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "s?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        }

        service.GetFieldByDocumentId = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/field"
            });
            return promise;
        }

        service.GetAtributeById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/data/"
            });
            return promise;
        }
        return service;

    }]);
    //hoant
    app.service('TaxonomyService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/taxonomy";
        var prefixCoreApiUrlVoca = "api/taxonomy/vocabularies";

        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "terms?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetTree = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/terms/tree"
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/terms/" + id
            });
            return promise;
        };

        service.GetByVocabularyId = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/vocabularies/" + id + "/terms"
            });
            return promise;
        };

        service.GetByParentId = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/terms/parent=" + id
            });
            return promise;
        };

        service.Create = function (id, model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl + "/vocabularies/" + id + "/terms",
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/terms/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/terms/" + id,
            });
            return promise;
        }

        //service.DeleteMany = function (model, isDeleteChild) {
        //    var promise = $http({
        //        method: 'DELETE',
        //        url: baseUrl + prefixCoreApiUrl,
        //        data: model
        //    });
        //    return promise;
        //}

        return service;

    }]);
    app.service('TaxonomyVocabularyService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/taxonomy/vocabularies";

        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl
            });
            return promise;
        }
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };
        service.GetByVocabularyId = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/terms"
            });
            return promise;
        };

        service.Create = function (id, model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        }

        //service.DeleteMany = function (model, isDeleteChild) {
        //    var promise = $http({
        //        method: 'DELETE',
        //        url: baseUrl + prefixCoreApiUrl,
        //        data: model
        //    });
        //    return promise;
        //}

        return service;

    }]);
    //hoant
    app.service('MetadataFieldService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/metadatafield";

        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        }
        return service;

    }]);
    app.service('MetadataTemplateService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/metadatatemplate";

        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        }

        service.Clone = function (id) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/clone"
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        }

        service.GetMetadataFieldByMetadataTemplateId = function (id) {
            if (id === "all") {
                id = "00000000-0000-0000-0000-000000000000";
            }
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/field/"
            });
            return promise;
        }
        return service;

    }]);
    app.service('InstructionDocumentApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;
        var apiInstructionDocument = constantsFactory.ApiInstructionDocument;

        //pagging
        service.Get = function (currentPage, pageSize, filterText, fondProfileId, approverId, isOrtherDocument, name) {
            filterText = filterText === undefined || filterText === null || filterText === "" ? "null" : '"' + filterText + '"';
            name = name === undefined || name === null || name === "" ? "null" : '"' + name + '"';
            fondProfileId = fondProfileId === undefined || fondProfileId === null || fondProfileId === "" ? "null" : '"' + fondProfileId + '"';
            approverId = approverId === undefined || approverId === null || approverId === "" ? "null" : '"' + approverId + '"';
            isOrtherDocument = isOrtherDocument === undefined || isOrtherDocument === null || isOrtherDocument === "" ? "null" : '"' + isOrtherDocument + '"';

            var filter = '{"PageSize":' + pageSize
                + ',"PageNumber":' + currentPage
                + ',"TextSearch":' + filterText
                + ',"FondProfileId":' + fondProfileId
                + ',"ApproverId":' + approverId
                + ',"IsOrtherDocument":' + isOrtherDocument
                + ',"Name":' + name
                + '}';
            var urlEncode = apiInstructionDocument + "/instructiondocument/query" + encodeURI('?filter=' + filter);
            var promise = $http({
                method: 'GET',
                url: basedUrl + urlEncode
            });
            return promise;
        };

        service.GetById = function (fondProfileId, instructionDocumentId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiInstructionDocument + "/" + fondProfileId + "/instructiondocument/" + instructionDocumentId
            });
            return promise;
        }
        service.GetByName = function (fondProfileId, name) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiInstructionDocument + "/" + fondProfileId + "/instructiondocument/" + name + "/byname",
            });
            return promise;
        }
        service.AddInstructionDocument = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + apiInstructionDocument + "/" + postData.FondProfileId + "/instructiondocument",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.EditInstructionDocument = function (putData) {

            var promise = $http({
                method: 'PUT',
                url: basedUrl + apiInstructionDocument + "/" + putData.FondProfileId + "/instructiondocument/" + putData.InstructionDocumentId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        service.UpdateStatus = function (putData) {

            var promise = $http({
                method: 'PUT',
                url: basedUrl + apiInstructionDocument + "/" + putData.FondProfileId + "/instructiondocument/" + putData.InstructionDocumentId + "/status",
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        service.DeleteInstructionDocument = function (fondProfileId, instructionDocumentId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiInstructionDocument + "/" + fondProfileId + "/instructiondocument/" + instructionDocumentId,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        // Return service
        return service;

    }]);
    app.service('EliminateDocumentApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;
        var apiEliminateDocument = constantsFactory.ApiEliminateDocument;

        //pagging
        service.Get = function (currentPage, pageSize, filterText, fondProfileId, hasFondProfile) {
            filterText = filterText === undefined || filterText === null || filterText === "" ? "null" : '"' + filterText + '"';
            fondProfileId = fondProfileId === undefined || fondProfileId === null || fondProfileId === "" ? "null" : '"' + fondProfileId + '"';
            hasFondProfile = hasFondProfile === undefined || hasFondProfile === null || hasFondProfile === "" ? false : +hasFondProfile;
            var filter = '{"PageSize":' + pageSize
                + ',"PageNumber":' + currentPage
                + ',"TextSearch":' + filterText
                + ',"FondProfileId":' + fondProfileId
                + ',"HasFondProfile":' + hasFondProfile
                + '}';
            var urlEncode = apiEliminateDocument + "/eliminatedocument/query" + encodeURI('?filter=' + filter);
            var promise = $http({
                method: 'GET',
                url: basedUrl + urlEncode
            });
            return promise;
        };

        service.GetById = function (fondProfileId, eliminateDocumentId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiEliminateDocument + "/" + fondProfileId + "/eliminatedocument/" + eliminateDocumentId
            });
            return promise;
        }
        service.GetByName = function (fondProfileId, name) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiEliminateDocument + "/" + fondProfileId + "/eliminatedocument/" + name + "/byname",
            });
            return promise;
        }
        service.GetByNumber = function (fondProfileId, number) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiEliminateDocument + "/" + fondProfileId + "/eliminatedocument/" + number + "/bynumber",
            });
            return promise;
        }
        service.GetPacketNumber = function (fondProfileId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiEliminateDocument + "/" + fondProfileId + "/eliminatedocument/bypacketnumber",
            });
            return promise;
        }

        service.AddEliminateDocument = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + apiEliminateDocument + "/" + postData.FondProfileId + "/eliminatedocument",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };
        service.EditEliminateDocument = function (putData) {

            var promise = $http({
                method: 'PUT',
                url: basedUrl + apiEliminateDocument + "/" + putData.FondProfileId + "/eliminatedocument/" + putData.EliminateDocumentId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        service.UpdateStatus = function (putData) {

            var promise = $http({
                method: 'PUT',
                url: basedUrl + apiEliminateDocument + "/" + putData.FondProfileId + "/eliminatedocument/" + putData.eliminateDocumentId + "/status",
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        service.DeleteEliminateDocument = function (fondProfileId, eliminateDocumentId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiEliminateDocument + "/" + fondProfileId + "/eliminatedocument/" + eliminateDocumentId,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        service.DeleteEliminateDocuments = function (fondProfileId, model) {
            var data = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiEliminateDocument + "/" + fondProfileId + "/eliminatedocument" + "?eliminatedocumentGuids=" + data,
            });
            return promise;
        };
        // Return service
        return service;

    }]);
    app.service('TrackBookSourceApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;
        var apiTrackBookSource = constantsFactory.ApiTrackBookSource;

        //pagging
        service.Get = function (currentPage, pageSize, filterText) {
            filterText = filterText === undefined || filterText === null || filterText === "" ? "null" : '"' + filterText + '"';
            var filter = '{"PageSize":' + pageSize
                + ',"PageNumber":' + currentPage
                + ',"TextSearch":' + filterText
                + '}';
            var urlEncode = apiTrackBookSource + "/query" + encodeURI('?filter=' + filter);
            var promise = $http({
                method: 'GET',
                url: basedUrl + urlEncode
            });
            return promise;
        };

        service.GetById = function (trackBookSourceId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiTrackBookSource + "/" + trackBookSourceId
            });
            return promise;
        }

        service.AddTrackBookSource = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + apiTrackBookSource,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };
        service.EditTrackBookSource = function (putData) {

            var promise = $http({
                method: 'PUT',
                url: basedUrl + apiTrackBookSource + "/" + putData.TrackBookSourceId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        service.DeleteTrackBookSource = function (trackBookSourceId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiTrackBookSource + "/" + trackBookSourceId,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        service.DeleteTrackBookSources = function (model) {
            var data = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiTrackBookSource + "?trackBookSourceGuids=" + data,
            });
            return promise;
        };
        return service;

    }]);
    app.service('WarehouseApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;
        var apiWareHouse = constantsFactory.ApiWareHouse;

        //pagging
        service.Get = function (currentPage, pageSize, filterText, fondId) {
            filterText = filterText === undefined || filterText === null || filterText === "" ? "null" : '"' + filterText + '"';
            currentPage = currentPage === undefined || currentPage === null || currentPage === "" ? "null" : '"' + currentPage + '"';
            pageSize = pageSize === undefined || pageSize === null || pageSize === "" ? "null" : '"' + pageSize + '"';
            fondId = fondId === undefined || fondId === null || fondId === "" ? "null" : '"' + fondId + '"';
            var filter = '{"PageSize":' + pageSize
                + ',"PageNumber":' + currentPage
                + ',"TextSearch":' + filterText
                + ',"FondId":' + fondId
                + '}';
            var urlEncode = apiWareHouse + "/query" + encodeURI('?filter=' + filter);
            var promise = $http({
                method: 'GET',
                url: basedUrl + urlEncode
            });
            return promise;
        };

        service.GetById = function (wareHouseId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiWareHouse + "/" + wareHouseId
            });
            return promise;
        }
        service.GetByCode = function (code) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiWareHouse + "/" + code + "/bycode"
            });
            return promise;
        }

        service.AddWareHouse = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + apiWareHouse,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };
        service.EditWareHouse = function (putData) {

            var promise = $http({
                method: 'PUT',
                url: basedUrl + apiWareHouse + "/" + putData.WareHouseId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        service.DeleteWareHouse = function (trackBookSourceId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiWareHouse + "/" + trackBookSourceId,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        service.DeleteWareHouses = function (model) {
            var data = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiWareHouse + "?trackBookSourceGuids=" + data,
            });
            return promise;
        };
        return service;

    }]);
    app.service('RoomApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;
        var apiWareHouseRoom = constantsFactory.ApiWareHouseRoom;

        //pagging
        service.Get = function (currentPage, pageSize, filterText, fondId, wareHouseId) {
            filterText = filterText === undefined || filterText === null || filterText === "" ? "null" : '"' + filterText + '"';
            currentPage = currentPage === undefined || currentPage === null || currentPage === "" ? "null" : '"' + currentPage + '"';
            pageSize = pageSize === undefined || pageSize === null || pageSize === "" ? "null" : '"' + pageSize + '"';
            fondId = fondId === undefined || fondId === null || fondId === "" ? "null" : '"' + fondId + '"';
            wareHouseId = wareHouseId === undefined || wareHouseId === null || wareHouseId === "" ? "null" : '"' + wareHouseId + '"';
            var filter = '{"PageSize":' + pageSize
                + ',"PageNumber":' + currentPage
                + ',"TextSearch":' + filterText
                + ',"FondId":' + fondId
                + ',"WareHouseId":' + wareHouseId
                + '}';
            var urlEncode = apiWareHouseRoom + "/query" + encodeURI('?filter=' + filter);
            var promise = $http({
                method: 'GET',
                url: basedUrl + urlEncode
            });
            return promise;
        };

        service.GetById = function (roomId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiWareHouseRoom + "/" + roomId
            });
            return promise;
        }
        service.GetByCode = function (code) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiWareHouseRoom + "/" + code + "/bycode"
            });
            return promise;
        }

        service.AddRoom = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + apiWareHouseRoom,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };
        service.EditRoom = function (putData) {

            var promise = $http({
                method: 'PUT',
                url: basedUrl + apiWareHouseRoom + "/" + putData.RoomId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        service.DeleteRoom = function (roomId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiWareHouseRoom + "/" + roomId,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        service.DeleteRooms = function (model) {
            var data = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiWareHouseRoom + "?roomGuids=" + data,
            });
            return promise;
        };
        return service;

    }]);
    app.service('ShelfDeckTypeApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;
        var apiShelfDeckType = constantsFactory.ApiShelfDeckType;

        //pagging
        service.Get = function (currentPage, pageSize, filterText) {
            filterText = filterText === undefined || filterText === null || filterText === "" ? "null" : '"' + filterText + '"';
            currentPage = currentPage === undefined || currentPage === null || currentPage === "" ? "null" : '"' + currentPage + '"';
            pageSize = pageSize === undefined || pageSize === null || pageSize === "" ? "null" : '"' + pageSize + '"';
            var filter = '{"PageSize":' + pageSize
                + ',"PageNumber":' + currentPage
                + ',"TextSearch":' + filterText
                + '}';
            var urlEncode = apiShelfDeckType + "/query" + encodeURI('?filter=' + filter);
            var promise = $http({
                method: 'GET',
                url: basedUrl + urlEncode
            });
            return promise;
        };

        service.GetById = function (roomId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiShelfDeckType + "/" + roomId
            });
            return promise;
        }
        service.GetByCode = function (code) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiShelfDeckType + "/" + code + "/bycode"
            });
            return promise;
        }

        service.AddShelfDeckType = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + apiShelfDeckType,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };
        service.EditShelfDeckType = function (putData) {

            var promise = $http({
                method: 'PUT',
                url: basedUrl + apiShelfDeckType + "/" + putData.ShelfDeckTypeId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        service.DeleteShelfDeckType = function (shelfDeckTypeId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiShelfDeckType + "/" + shelfDeckTypeId,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        service.DeleteShelfDeckTypes = function (model) {
            var data = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiShelfDeckType + "?shelfDeckTypeGuids=" + data,
            });
            return promise;
        };
        return service;

    }]);
    app.service('CouponWoodblockApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;
        var apiCouponWoodblock = constantsFactory.ApiCouponWoodblock;

        //pagging
        service.Get = function (currentPage, pageSize, filterText, hasAttribule, setOfBookId, bookId) {
            filterText = filterText === undefined || filterText === null || filterText === "" ? "null" : '"' + filterText + '"';
            setOfBookId = setOfBookId === undefined || setOfBookId === null || setOfBookId === "" ? "null" : '"' + setOfBookId + '"';
            bookId = bookId === undefined || bookId === null || bookId === "" ? "null" : '"' + bookId + '"';
            hasAttribule = hasAttribule === undefined || hasAttribule === null || hasAttribule === "" ? false : '"' + hasAttribule + '"';
            var filter = '{"PageSize":' + pageSize
                + ',"PageNumber":' + currentPage
                + ',"TextSearch":' + filterText
                + ',"HasAttribule":' + hasAttribule
                + ',"SetOfBookId":' + setOfBookId
                + ',"BookId":' + bookId
                + '}';
            var urlEncode = apiCouponWoodblock + "/query" + encodeURI('?filter=' + filter);
            var promise = $http({
                method: 'GET',
                url: basedUrl + urlEncode
            });
            return promise;
        };

        service.GetById = function (couponWoodblockId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiCouponWoodblock + "/" + couponWoodblockId
            });
            return promise;
        }
        service.GetByCouponNumber = function (couponNumber) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiCouponWoodblock + "/" + couponNumber + "/bycouponnumber"
            });
            return promise;
        }

        service.AddCouponWoodblock = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + apiCouponWoodblock,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };
        service.EditCouponWoodblock = function (putData) {

            var promise = $http({
                method: 'PUT',
                url: basedUrl + apiCouponWoodblock + "/" + putData.CouponWoodblockId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        service.DeleteCouponWoodblock = function (couponWoodblockId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiCouponWoodblock + "/" + couponWoodblockId,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        service.DeleteCouponWoodblocks = function (model) {
            var data = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiCouponWoodblock + "?couponWoodblockGuids=" + data,
            });
            return promise;
        };
        return service;

    }]);
    app.service('TableContentWoodblockApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;
        var apiTableContentWoodblock = constantsFactory.ApiTableContentWoodblock;

        //pagging
        service.Get = function (currentPage, pageSize, filterText, hasAttribule, setOfBookId, bookId, sheetNumber) {
            filterText = filterText === undefined || filterText === null || filterText === "" ? "null" : '"' + filterText + '"';
            setOfBookId = setOfBookId === undefined || setOfBookId === null || setOfBookId === "" ? "null" : '"' + setOfBookId + '"';
            bookId = bookId === undefined || bookId === null || bookId === "" ? "null" : '"' + bookId + '"';
            sheetNumber = sheetNumber === undefined || sheetNumber === null || sheetNumber === "" ? "null" : '"' + sheetNumber + '"';
            hasAttribule = hasAttribule === undefined || hasAttribule === null || hasAttribule === "" ? false : '"' + hasAttribule + '"';
            var filter = '{"PageSize":' + pageSize
                + ',"PageNumber":' + currentPage
                + ',"TextSearch":' + filterText
                + ',"HasAttribule":' + hasAttribule
                + ',"SetOfBookId":' + setOfBookId
                + ',"BookId":' + bookId
                + ',"SheetNumber":' + sheetNumber
                + '}';
            var urlEncode = apiTableContentWoodblock + "/query" + encodeURI('?filter=' + filter);
            var promise = $http({
                method: 'GET',
                url: basedUrl + urlEncode
            });
            return promise;
        };

        service.GetById = function (tableContentId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiTableContentWoodblock + "/" + tableContentId
            });
            return promise;
        }

        service.AddTableContentWoodblock = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + apiTableContentWoodblock,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };
        service.EditTableContentWoodblock = function (putData) {

            var promise = $http({
                method: 'PUT',
                url: basedUrl + apiTableContentWoodblock + "/" + putData.tableContentId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        service.DeleteTableContentWoodblock = function (tableContentId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiTableContentWoodblock + "/" + tableContentId,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        service.DeleteTableContentWoodblocks = function (model) {
            var data = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiTableContentWoodblock + "?tableContentGuids=" + data,
            });
            return promise;
        };
        return service;

    }]);
    app.service('DocumentAttachmentApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;
        var apiAttachmentDocument = constantsFactory.ApiAttachmentDocument;
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiAttachmentDocument + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.DeleteById = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiAttachmentDocument + id,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };

        service.DeleteAttachmentDocument = function (attachmentId, nodeId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiAttachmentDocument + "/" + attachmentId + "/" + nodeId + "/nodes",
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };

        service.AddAttachmentDocument = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "api/v2/arm/documents/attachments",
                data: postData

            });
            return promise;
        };

        service.UpdateAttachmentDocument = function (model) {
            var promise = $http({
                method: 'PUT',
                url: basedUrl + "api/v2/arm/documents/attachments/" + model.Id,
                data: model

            });
            return promise;
        };
        service.DeleteAttachmentDocument = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + "api/v2/arm/attachments/" + id,
            });
            return promise;
        };
        service.GetAttachmentDocumentById = function (Id) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + 'api/v2/arm/attachments/' + Id + '/guid',

            });
            return promise;
        };

        service.DeleteAttachmentDocumentInMdmByMdmNodeId = function (nodeId) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + "api/v2/core/nodes/delete/" + nodeId,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };

        // Return service
        return service;

    }]);
    app.service('ReaderProfileApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;
        var apiReaderProfile = constantsFactory.ApiReaderProfile;

        //pagging
        service.Get = function (currentPage, pageSize, filterText, nationalityTermId) {

            filterText = filterText === undefined || filterText === null || filterText === "" ? "null" : '"' + filterText + '"';
            nationalityTermId = nationalityTermId === undefined || nationalityTermId === null || nationalityTermId === "" ? "null" : '"' + nationalityTermId + '"';

            var filter = '{"PageSize":' + pageSize
                + ',"PageNumber":' + currentPage
                + ',"TextSearch":' + filterText
                + ',"NationalityTermId":' + nationalityTermId
                + '}';
            var urlEncode = apiReaderProfile + "/query" + encodeURI('?filter=' + filter);
            var promise = $http({
                method: 'GET',
                url: basedUrl + urlEncode
            });
            return promise;
        };

        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiReaderProfile + "/query?filter=" + stringFilter
            });
            return promise;
        }
        service.GetById = function (readerProfileId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiReaderProfile + "/" + readerProfileId,
            });
            return promise;
        }
        service.GetByName = function (name) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiReaderProfile + "/" + name + "/byname",
            });
            return promise;
        }
        service.GetByIdNumber = function (idNumber) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiReaderProfile + "/" + idNumber + "/byidnumber",
            });
            return promise;
        }
        service.GetByEmail = function (email) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiReaderProfile + "/" + email + "/byemail",
            });
            return promise;
        }
        service.GetByReaderAccountId = function (readerAccountId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiReaderProfile + "/" + readerAccountId + "/byreaderaccountid",
            });
            return promise;
        }
        service.GetByUserName = function (userName) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiReaderProfile + "/" + userName + "/byusername",
            });
            return promise;
        }
        service.AddReaderProfile = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + apiReaderProfile,
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.EditReaderProfile = function (putData) {

            var promise = $http({
                method: 'PUT',
                url: basedUrl + apiReaderProfile + "/" + putData.ReaderId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        service.DeleteReaderProfile = function (readerProfileId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiReaderProfile + "/" + readerProfileId,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        service.DeleteReaderProfiles = function (model) {
            var data = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiReaderProfile + "?readerGuids=" + data,
            });
            return promise;
        };
        return service;
    }]);
    app.service('ReaderAccountApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;
        var apiReaderProfile = constantsFactory.ApiReaderProfile;

        //pagging
        service.Get = function (currentPage, pageSize, filterText, readerId) {

            filterText = filterText === undefined || filterText === null || filterText === "" ? "null" : '"' + filterText + '"';
            readerId = readerId === undefined || readerId === null || readerId === "" ? "null" : '"' + readerId + '"';

            var filter = '{"PageSize":' + pageSize
                + ',"PageNumber":' + currentPage
                + ',"TextSearch":' + filterText
                + ',"ReaderId":' + readerId
                + '}';
            var urlEncode = apiReaderProfile + "/" + readerId + "/readeraccount/query" + encodeURI('?filter=' + filter);
            var promise = $http({
                method: 'GET',
                url: basedUrl + urlEncode
            });
            return promise;
        };

        service.GetById = function (readerProfileId, readerAccountId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiReaderProfile + "/" + readerProfileId + "/readeraccount/" + readerAccountId,
            });
            return promise;
        }
        service.GetByPassword = function (readerProfileId, readerAccountId, password) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiReaderProfile + "/" + readerProfileId + "/readeraccount/" + readerAccountId + "/" + password + "/bypass",
            });
            return promise;
        }
        service.GetByUserName = function (readerProfileId, userName) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + apiReaderProfile + "/" + readerProfileId + "/readeraccount/" + userName + "/byname",
            });
            return promise;
        }
        service.AddReaderAccount = function (postData) {
            var promise = $http({
                method: 'POST',
                url: basedUrl + apiReaderProfile + "/" + postData.ReaderId + "/readeraccount",
                headers: {
                    'Content-type': ' application/json'
                },
                data: postData
            });
            return promise;
        };

        service.EditReaderAccount = function (putData) {

            var promise = $http({
                method: 'PUT',
                url: basedUrl + apiReaderProfile + "/" + putData.ReaderId + "/readeraccount",
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        service.EditReaderAccountPassword = function (putData) {

            var promise = $http({
                method: 'PUT',
                url: basedUrl + apiReaderProfile + "/" + putData.ReaderId + "/readeraccount" + "/" + putData.ReaderAccountId,
                headers: {
                    'Content-type': ' application/json'
                },
                data: putData
            });
            return promise;
        };
        service.DeleteReaderProfile = function (readerProfileId, readerAccountId) {
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiReaderProfile + "/" + readerProfileId + "/readeraccount/" + readerAccountId,
                headers: {
                    'Content-type': ' application/json'
                },
            });
            return promise;
        };
        service.DeleteReaderProfiles = function (readerProfileId, model) {
            var data = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: basedUrl + apiReaderProfile + "/" + readerProfileId + "/readeraccount" + "?readerAccountGuids=" + data,
            });
            return promise;
        };
        return service;
    }]);
    app.service('CatalogService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/catalogs/catalogmaster";
        service.ApiUrl = prefixCoreApiUrl;
        service.GetApiLoadItemListSrc = function (catalogCode) {
            return "[[configDomain]]/" + prefixCoreApiUrl + "/" + catalogCode + "/itemlist";
        }
        service.GetApiLoadItemTreeSrc = function (catalogCode) {
            return "[[configDomain]]/" + prefixCoreApiUrl + "/" + catalogCode + "/itemstree";
        }
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetAll = function () {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter={}"
            });
            return promise;
        }
        service.GetTree = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/tree"
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        }



        service.GetItemTree = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/itemtree"
            });
            return promise;
        };
        service.GetItemList = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/itemlist"
            });
            return promise;
        };

        //api/catalogs/catalogmaster/{id}/itembylevel
        service.GetItemByLevel = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/itembylevel"
            });
            return promise;
        };
        return service;

    }]);
    app.service('CatalogItemService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/catalogitem";
        service.apiUrl = prefixCoreApiUrl;
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        }


        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id, isDeleteChild) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteManyLoki = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        }
        service.DeleteMany = function (model) {
            //var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl + "/delmany",
                data: model,
            });
            return promise;
        }


        service.GetAtributeById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id + "/data/"
            });
            return promise;
        }

        return service;

    }]);
    app.service('DemoService', ['$http', 'constantsFactory', '$q', function ($http, constantsFactory, $q) {

        var service = {};
        var createitem = function () {
            return {
                "Name": "Item-" + Math.floor((Math.random() * 100) + 1),
                "Number": Math.floor((Math.random() * 100) + 1),
                "Date": new Date(Math.random()),
                "Status": Math.random() >= 0.5,
            }
        }
        service.GetFilter = function (model) {

            var deferred = $q.defer();
            setTimeout(function () {
                var result = {
                    "Status": 1,
                    "Message": "",
                    "Data": [],
                    "TotalCount": 1000,
                }
                for (var i = 0; i < model.PageSize; i++) {
                    result.Data.push(createitem());
                }
                deferred.resolve(result);
            }, 1000);

            return deferred.promise;
        }
        return service;

    }]);
    app.service('ReaderRecordRegistrationService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/v2/arm/reader-record-registration";

        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        }
        service.ProcessDocument = function (documentId, requestData) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl + '/workflow/' + documentId + "/process",
                data: requestData
            });
            return promise;
        };
        service.GenarateFormNumber = function (formNumber) {
            var formNumberStr = "1";
            if (typeof (formNumber) !== undefined && formNumber != null) {
                formNumberStr = "" + formNumber;
            }
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/genformnumber/" + formNumberStr,
            });
            return promise;
        }
        service.GetExportExelLink = function (organizationId, tenBaoCao, organizationCode) {
            return baseUrl + prefixCoreApiUrl + "/export/" + organizationId + "/" + tenBaoCao + "/" + organizationCode;
        };
        return service;

    }]);
    app.service('ReaderRecordRequestService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/v2/arm/reader-record-request";
        service.GetFilterItem = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '-item' + "?stringFilter=" + stringFilter
            });
            return promise;
        }
        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        }
        service.ProcessDocument = function (documentId, requestData) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl + '/workflow/' + documentId + "/process",
                data: requestData
            });
            return promise;
        };

        service.GenarateFormNumber = function (formNumber) {
            var formNumberStr = "1";
            if (typeof (formNumber) !== undefined && formNumber != null) {
                formNumberStr = "" + formNumber;
            }
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/genformnumber/" + formNumberStr,
            });
            return promise;
        }

        service.GetExportExelLink = function (organizationId, tenBaoCao, organizationCode) {
            return baseUrl + prefixCoreApiUrl + "/export/" + organizationId + "/" + tenBaoCao + "/" + organizationCode;
        }
        service.GetExportExelLinkReadStorage = function (organizationId, tenBaoCao, organizationCode) {
            return baseUrl + prefixCoreApiUrl + "/export-read-storage/" + organizationId + "/" + tenBaoCao + "/" + organizationCode;
        }
        service.GetExportExelLinkReadReader = function (organizationId, tenBaoCao, organizationCode) {
            return baseUrl + prefixCoreApiUrl + "/export-read-reader/" + organizationId + "/" + tenBaoCao + "/" + organizationCode;
        }
        return service;

    }]);
    app.service('ReaderRecordCopyService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/v2/arm/reader-record-copy";


        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        };

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        };

        service.Delete = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        };

        service.DeleteMany = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        };
        service.ProcessDocument = function (documentId, requestData) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl + '/workflow/' + documentId + "/process",
                data: requestData
            });
            return promise;
        };

        service.GenarateFormNumber = function (formNumber) {
            var formNumberStr = "1";
            if (typeof (formNumber) !== undefined && formNumber != null) {
                formNumberStr = "" + formNumber;
            }
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/genformnumber/" + formNumberStr,
            });
            return promise;
        };

        service.GetExportExelLink = function (organizationId, tenBaoCao, organizationCode) {
            return baseUrl + prefixCoreApiUrl + "/export/" + organizationId + "/" + tenBaoCao + "/" + organizationCode;
        }
        service.GetExportExelLinkTransfer = function (organizationId, tenBaoCao, organizationCode) {
            return baseUrl + prefixCoreApiUrl + "/export-transfer/" + organizationId + "/" + tenBaoCao + "/" + organizationCode;
        }

        return service;

    }]);
    app.service('DatabaseBackupRestoreService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixApiUrl = "api/system/database-backup";

        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id
            });
            return promise;
        };

        service.Backup = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl + "/backup",
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl + "/deletemany",
                data: model
            });
            return promise;
        }

        service.InitCardCode = function (parentOrganizationId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/init-code?parentOrganizationId=" + parentOrganizationId,
            });
            return promise;
        }

        service.AddExtendDuration = function (extendModel) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + "api/v2/arm/reader-cards-extend",
                data: extendModel
            });
            return promise;
        };

        service.GetListCardExtendByCardId = function (cardId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + "api/v2/arm/reader-cards-extend/" + cardId
            });
            return promise;
        };

        service.DeleteCardExtendById = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + "api/v2/arm/reader-cards-extend" + "/" + id,
            });
            return promise;
        }

        return service;

    }]);
    app.service('ReaderCardService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixApiUrl = "api/v2/arm/reader-cards";

        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        }

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/" + id
            });
            return promise;
        };

        service.Add = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl,
                data: model
            });
            return promise;
        }

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixApiUrl + "/" + id,
                data: model
            });
            return promise;

        }

        service.Delete = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixApiUrl + "/" + id,
            });
            return promise;
        }

        service.DeleteMany = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixApiUrl + "/deletemany",
                data: model
            });
            return promise;
        }

        service.InitCardCode = function (parentOrganizationId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixApiUrl + "/init-code?parentOrganizationId=" + parentOrganizationId,
            });
            return promise;
        }

        service.AddExtendDuration = function (extendModel) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + "api/v2/arm/reader-cards-extend",
                data: extendModel
            });
            return promise;
        };

        service.GetListCardExtendByCardId = function (cardId) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + "api/v2/arm/reader-cards-extend/" + cardId
            });
            return promise;
        };

        service.DeleteCardExtendById = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + "api/v2/arm/reader-cards-extend" + "/" + id,
            });
            return promise;
        }

        return service;

    }]);
    app.service('ReaderRecordValidateService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/v2/arm/reader-record-validate";
        service.GetFilterItem = function (model) {
            var stringFilter = angular.toJson(model);

            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + '-item' + "?stringFilter=" + stringFilter
            });
            return promise;
        };

        service.UpdateMultiItem = function (model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "-item/multi",
                data: model
            });
            return promise;

        };
        service.UpdateItem = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "-item/" + id,
                data: model
            });
            return promise;

        };

        service.GenarateValidateNumber = function (validateNumber) {
            var validateNumberStr = "1";
            if (typeof (validateNumber) !== undefined && validateNumber != null) {
                validateNumberStr = "" + validateNumber;
            }
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "-item/genvalidatenumber/" + validateNumberStr,
            });
            return promise;
        };

        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        };

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        };

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        };

        service.Delete = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        };

        service.DeleteMany = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        };

        service.ProcessDocument = function (documentId, requestData) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl + '/workflow/' + documentId + "/process",
                data: requestData
            });
            return promise;
        };

        service.GenarateFormNumber = function (formNumber) {
            var formNumberStr = "1";
            if (typeof (formNumber) !== undefined && formNumber != null) {
                formNumberStr = "" + formNumber;
            }
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/genformnumber/" + formNumberStr,
            });
            return promise;
        };

        service.GetExportExelLink = function (organizationId, tenBaoCao, organizationCode) {
            return baseUrl + prefixCoreApiUrl + "/export/" + organizationId + "/" + tenBaoCao + "/" + organizationCode;
        };
        service.GetExportExelLinkTransfer = function (organizationId, tenBaoCao, organizationCode) {
            return baseUrl + prefixCoreApiUrl + "/export-transfer/" + organizationId + "/" + tenBaoCao + "/" + organizationCode;
        };
        return service;

    }]);
    app.service('NavigationApiService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};

        var basedUrl = constantsFactory.ApiUrl;

        service.GetNavigationByRole = function (roleId) {
            var promise = $http({
                method: 'GET',
                url: basedUrl + 'api/' + roleId + '/navigation'
            });
            return promise;
        };
        service.GetAllNavigationTree = function () {
            var promise = $http({
                method: 'GET',
                url: basedUrl + 'api/' + roleId + '/navigation/all'
            });
            return promise;
        };
        // Return service
        return service;

    }]);
    app.service('ReaderNotificationService', ['$http', 'constantsFactory', function ($http, constantsFactory) {

        var service = {};
        var baseUrl = constantsFactory.ApiUrl;
        var prefixCoreApiUrl = "api/v2/arm/reader-notification";



        service.GetFilter = function (model) {
            var stringFilter = angular.toJson(model);
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "?stringFilter=" + stringFilter
            });
            return promise;
        };

        service.GetById = function (id) {
            var promise = $http({
                method: 'GET',
                url: baseUrl + prefixCoreApiUrl + "/" + id
            });
            return promise;
        };

        service.Create = function (model) {
            var promise = $http({
                method: 'POST',
                url: baseUrl + prefixCoreApiUrl,
                data: model
            });
            return promise;
        };

        service.Update = function (id, model) {
            var promise = $http({
                method: 'PUT',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
                data: model
            });
            return promise;

        };

        service.Delete = function (id) {
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "/" + id,
            });
            return promise;
        };

        service.DeleteMany = function (model) {
            var deleteData = angular.toJson(model);
            var promise = $http({
                method: 'DELETE',
                url: baseUrl + prefixCoreApiUrl + "?deleteData=" + deleteData,
            });
            return promise;
        };
        return service;

    }]);
});