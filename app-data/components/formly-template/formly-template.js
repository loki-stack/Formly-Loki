﻿/* SAVIS Vietnam Corporation
 *
 * This module is to separate configuration of authentication to app.js
 * NguyenBv - Jan 2017
 */

define(["angularAMD", "jquery", "components/service/amdservice",
    "ngfileupload"
], function (angularAMD, jQuery) {
    "use strict";
    /* Init the auth config, interceptor, and runtime modules */


    var factory = {};


    factory.init = function (app) {
        app.run(function (formlyConfig, formlyValidationMessages) {
            //tạo message validate
            formlyConfig.setWrapper({
                name: "validation",
                templateUrl: "app-data/components/formly-template/wraper/error-message.html"
            });
            formlyConfig.setWrapper({
                name: "loader",
                templateUrl: "app-data/components/formly-template/wraper/loader.html"
            });
            formlyConfig.setWrapper({
                name: "range",
                templateUrl: "app-data/components/formly-template/wraper/range.html"
            });
            formlyConfig.setWrapper({
                name: "color",
                templateUrl: "app-data/components/formly-template/wraper/color.html"
            });
            //horizontal stuff
            formlyConfig.setWrapper({
                name: "horizontalLabel",
                templateUrl: "app-data/components/formly-template/wraper/horizontalLabel.html"
            });

            //layout
            formlyConfig.setWrapper({
                name: "layout",
                templateUrl: "app-data/components/formly-template/wraper/layout.html"
            });
            //layout
            formlyConfig.setWrapper({
                name: "table",
                templateUrl: "app-data/components/formly-template/wraper/table.html"
            });

            //Cấu hình in message lỗi
            formlyValidationMessages.addTemplateOptionValueMessage("minlength", "minlength", "Tối thiểu có :", " ký tự", "");
            formlyValidationMessages.addTemplateOptionValueMessage("maxlength", "maxlength", "Tối đa có :", " ký tự", "");
            formlyValidationMessages.addTemplateOptionValueMessage("min", "min", "Giá trị tối thiểu là :", "", "");
            formlyValidationMessages.addTemplateOptionValueMessage("max", "max", "Giá trị tối đa là :", "", "");
            formlyValidationMessages.addStringMessage('required', 'Trường bắt buộc');

        });
        /*Wraper---------------------------------*/
        app.run(function (formlyConfig) {




        });


        /*Control---------------------------------------------------------------------*/
        app.run(function (formlyConfig, $timeout, $http) {
            //reset all control



            //Savis Input
            formlyConfig.setType({
                "name": "savis-input",
                "extends": "input",
                "wrapper": ["validation"],
                "defaultOptions": {
                    "ngModelElAttrs": {
                        "class": 'form-control input-sm'
                    },
                    "className": "col-md-12",
                    "templateOptions": {
                        "type": "text",
                        "label": "text",
                        "labelSize": "20%",
                        "controlSize": "80%",
                        "required": true,
                        "disabled": false,
                        "placeholder": "Nhập vào text",
                        //Validate stuff-----------------------------------------------------------------------------//
                        "isValidateByApi": false, //lựa chọn validate bằng api
                        "apiValidate": "http://localhost:9800/api/cmsstoryline/$viewValue%", //api sử dụng validate| với $viewValue là dữ liệu ô nhập| option $model.key là giá trị dữ liệu liên quan
                        "apiValidateCondition": "if($response){return true;}else{return false;}", //điều kiện check validate với $response là dữ liệu trả về
                        "apiValidateError": "Error Api Validate", //message thông báo lỗi
                        //Phục vụ validate asyn
                        "loading": false,
                        "onKeydown": function (value, options) {
                            options.validation.show = false;
                        },
                        "onBlur": function (value, options) {
                            options.validation.show = null;
                        },
                        //Parttern
                        "isValidateByParttern": false, //lựa chọn validate bằng Parttern
                        "partternValidate": "", //chuỗi Partern vd /validate/
                        "partternValidateError": "Error Parttern Validate", //message thông báo lỗi
                        //Custom Validate
                        "isValidateByCustom": false, //lựa chọn validate custom
                        "customValidate": "", //câu điều kiện
                        "customValidateError": "Error Custom Validate", //message báo lỗi
                        //Expresstion stuff---------------------------------------------------------------//

                        //hideExpression
                        "isUseHideExpression": false, //lựa chọn validate custom
                        "hideExpression": "", //câu điều kiện
                        //hideExpression
                        "isUseDisabledExpression": false, //lựa chọn validate custom
                        "disabledExpression": "", //câu điều kiện
                    },

                    "asyncValidators": {
                        "apiValidate": {
                            "expression": function ($viewValue, $modelValue, $scope) {
                                if ($scope.options.templateOptions.isValidateByApi) {

                                    $scope.options.templateOptions.loading = true;

                                    var promise = $http({
                                        method: "GET",
                                        url: $scope.options.templateOptions.apiValidate.replace("$viewValue%", $viewValue),
                                    });
                                    return promise.then(function (response) {
                                        var customFunction = Function("$response", "$viewValue", "$modelValue", "$scope",
                                            $scope.options.templateOptions.apiValidateCondition);
                                        var result = customFunction(response, $viewValue, $viewValue, $scope);
                                        if (!result) {
                                            throw new Error("SU go here");
                                        }
                                    });
                                } else {
                                    return $timeout(function () { });
                                }
                            },
                            "message": "to.apiValidateError",
                        },

                    },
                    "validators": {
                        "partternValidate": {
                            "expression": function ($viewValue, $modelValue, $scope) {
                                if ($scope.options.templateOptions.isValidateByParttern) {
                                    var value = $modelValue || $viewValue;
                                    var patt = new RegExp($scope.options.templateOptions.partternValidate);
                                    return patt.test(value);
                                } else {
                                    return true;
                                }

                            },
                            "message": "to.partternValidateError",
                        },
                        "customValidate": {
                            "expression": function ($viewValue, $modelValue, $scope) {
                                if ($scope.options.templateOptions.isValidateByCustom) {
                                    var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.customValidate);
                                    return customFunction($viewValue, $viewValue, $scope);
                                } else {
                                    return true;
                                }
                            },
                            "message": "to.customValidateError",
                        }
                    },
                    "modelOptions": {
                        "updateOn": "blur"
                    },
                    "expressionProperties": {
                        "templateOptions.disabled": function ($viewValue, $modelValue, $scope) {
                            if ($scope.options.templateOptions.isUseDisabledExpression) {
                                var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.disabledExpression);
                                return customFunction($viewValue, $viewValue, $scope);
                            } else {
                                return false;
                            }
                        },
                        //"hide": function ($viewValue, $modelValue, $scope) {
                        //    if ($scope.options.templateOptions.isUseHideExpression) {
                        //        var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.hideExpression);
                        //        return customFunction($viewValue, $viewValue, $scope);
                        //    } else {
                        //        return false;
                        //    }
                        //},
                    },


                },
                "controller": ["$scope", "$timeout", function ($scope, $timeout) {
                    if (!Array.isArray($scope.options.wrapper)) {
                        $scope.options.wrapper = [];
                    }
                    if ($scope.options.templateOptions.horizontalLabel) {
                        var index = $scope.options.wrapper.indexOf("horizontalLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("horizontalLabel");
                    } else {
                        var index = $scope.options.wrapper.indexOf("bootstrapLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("bootstrapLabel");
                    }
                    if ($scope.options.templateOptions.type == "date") {
                        $timeout(function () {
                            $scope.model[$scope.options.key] = new Date($scope.model[$scope.options.key]);
                        });

                    }


                }]
            });
            //Savis textarea
            formlyConfig.setType({
                "name": "savis-textarea",
                "extends": "textarea",
                "wrapper": ["validation"],
                "defaultOptions": {
                    "className": "col-md-12",
                    "templateOptions": {
                        "type": "text",
                        "label": "text",
                        "labelSize": "20%",
                        "controlSize": "80%",
                        "placeholder": "",
                        "label": "",
                        "line": 5,
                        //hideExpression
                        "isUseHideExpression": false, //lựa chọn validate custom
                        "hideExpression": "", //câu điều kiện
                        //hideExpression
                        "isUseDisabledExpression": false, //lựa chọn validate custom
                        "disabledExpression": "", //câu điều kiện
                    },
                    "expressionProperties": {
                        'templateOptions.disabled': function ($viewValue, $modelValue, $scope) {
                            if ($scope.options.templateOptions.isUseDisabledExpression) {
                                var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.disabledExpression);
                                return customFunction($viewValue, $viewValue, $scope);
                            } else {
                                return false;
                            }
                        },
                        //'hide': function ($viewValue, $modelValue, $scope) {
                        //    if ($scope.options.templateOptions.isUseHideExpression) {
                        //        var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.hideExpression);
                        //        return customFunction($viewValue, $viewValue, $scope);
                        //    } else {
                        //        return false;
                        //    }
                        //},
                    },
                    "controller": ["$scope", "$timeout", function ($scope, $timeout) {
                        if (!Array.isArray($scope.options.wrapper)) {
                            $scope.options.wrapper = [];
                        }
                        if ($scope.options.templateOptions.horizontalLabel) {
                            var index = $scope.options.wrapper.indexOf("horizontalLabel");
                            if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                            $scope.options.wrapper.push("horizontalLabel");
                        } else {
                            var index = $scope.options.wrapper.indexOf("bootstrapLabel");
                            if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                            $scope.options.wrapper.push("bootstrapLabel");
                        }
                    }]
                }

            });
            //ck-editer
            formlyConfig.setType({
                "name": "ckeditor",
                "wrapper": ["validation"],
                "extends": "savis-textarea",
                "template": "<textarea id=\"{{::id}}\" name=\"{{::id}}\" ckeditor ng-model=\"model[options.key]\"></textarea>",
            });
            //Savis dropdown
            formlyConfig.setType({
                "name": "savis-dropdown",
                "extends": "select",
                "wrapper": ["validation"],
                "defaultOptions": {
                    "className": "col-md-12",
                    "templateOptions": {
                        "labelSize": "20%",
                        "controlSize": "80%",
                        "required": true,
                        "disabled": false,
                        "placeholder": "Nhập vào text",
                        "label": "select",
                        "valueProp": "value",
                        "labelProp": "name",
                        "groupProp": "group",
                        "options": [{
                            value: "1",
                            name: "Option1"
                        }, {
                            value: "2",
                            name: "Option2"
                        }],
                        //Lấy các option từ api
                        "isGetOptionByApi": false,
                        "apiGetOption": "https://jsonplaceholder.typicode.com/comments?postId=$references",
                        "apiResponseData": '$response',


                        //hideExpression
                        "isUseHideExpression": false,
                        "hideExpression": "",
                        //hideExpression
                        "isUseDisabledExpression": false,
                        "disabledExpression": "",
                    },

                    "expressionProperties": {
                        "templateOptions.disabled": function ($viewValue, $modelValue, $scope) {
                            if ($scope.options.templateOptions.isUseDisabledExpression) {
                                var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.disabledExpression);
                                return customFunction($viewValue, $viewValue, $scope);
                            } else {
                                return false;
                            }
                        },
                        //"hide": function ($viewValue, $modelValue, $scope) {
                        //    if ($scope.options.templateOptions.isUseHideExpression) {
                        //        var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.hideExpression);
                        //        return customFunction($viewValue, $viewValue, $scope);
                        //    } else {
                        //        return false;
                        //    }
                        //},
                    },
                },
                "controller": ["$scope", "$timeout", function ($scope, $timeout) {
                    if (!Array.isArray($scope.options.wrapper)) {
                        $scope.options.wrapper = [];
                    }
                    if ($scope.options.templateOptions.horizontalLabel) {
                        var index = $scope.options.wrapper.indexOf("horizontalLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("horizontalLabel");
                    } else {
                        var index = $scope.options.wrapper.indexOf("bootstrapLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("bootstrapLabel");
                    }

                    //if (!$scope.options.templateOptions.required) {
                    //    $timeout(function () {
                    //        var defaultValue = {};
                    //        defaultValue[$scope.to.valueProp] = null;
                    //        defaultValue[$scope.to.labelProp] = "-- " + $scope.to.placeholder + " --";
                    //        $scope.to.options.unshift(defaultValue);
                    //        //$scope.model[$scope.options.key] = null;
                    //    });
                    //}
                    //hàm load động dữ liệu
                    var getOptionByApi = function (apiUrl, selectPropResponse) {
                        $scope.to.options = [];
                        var promise = $http({
                            method: 'GET',
                            url: apiUrl,
                        });
                        promise.success(function (response) {
                            $scope.to.options = [];

                            //if (!$scope.options.templateOptions.required) {
                            //    var defaultValue = {};
                            //    defaultValue[$scope.to.valueProp] = null;
                            //    defaultValue[$scope.to.labelProp] = "-- " + $scope.to.placeholder + " --";
                            //    $scope.to.options.unshift(defaultValue);
                            //    //$scope.model[$scope.options.key] = null;
                            //}

                            if (selectPropResponse === "$response") {
                                if (Array.isArray(response)) {
                                    $scope.to.options = response;
                                    if (!$scope.options.templateOptions.required) {
                                        //$timeout(function () {
                                        //    var defaultValue = {};
                                        //    defaultValue[$scope.to.valueProp] = null;
                                        //    defaultValue[$scope.to.labelProp] = "-- " + $scope.to.placeholder + " --";
                                        //    $scope.to.options.unshift(defaultValue);
                                        //    //$scope.model[$scope.options.key] = null;
                                        //});
                                    }
                                }
                            } else {
                                var atribute = selectPropResponse.replace("$response.", "");
                                if (Array.isArray(response[atribute])) {
                                    $scope.to.options = response[atribute];
                                    if (!$scope.options.templateOptions.required) {
                                        //$timeout(function () {
                                        //    var defaultValue = {};
                                        //    defaultValue[$scope.to.valueProp] = null;
                                        //    defaultValue[$scope.to.labelProp] = "-- " + $scope.to.placeholder + " --";
                                        //    $scope.to.options.unshift(defaultValue);
                                        //    //$scope.model[$scope.options.key] = null;
                                        //});
                                    }
                                }
                            }


                        });
                        return promise;
                    };

                    if ($scope.to.isGetOptionByApi) {

                        //Solution  | watch all :)) but not this 
                        //lấy ra danh sách key
                        var re = /\$([^\%])+\%/g;
                        var s = $scope.to.apiGetOption;
                        var listWatcher = [];
                        var listWatcherModel = [];
                        var m;
                        do {
                            m = re.exec(s);
                            if (m) {
                                var key = m[0].replace("$", "");
                                key = key.replace("%", "");
                                listWatcher.push(key);
                                listWatcherModel.push("model." + key);
                            }
                        } while (m);

                        //end---lấy ra danh sách key
                        if (listWatcherModel.length > 0) {
                            var a = angular.toJson(listWatcherModel)
                            a = a.replace(/\"/g, '');
                            $scope.$watchCollection(a, function (newValue, oldValue) {
                                if (newValue !== oldValue) {
                                    var apiUrl = $scope.to.apiGetOption;
                                    if ($scope.model[$scope.options.key] && oldValue) {
                                        $scope.model[$scope.options.key] = '';
                                    }
                                    for (var i = 0; i < listWatcher.length; i++) {
                                        apiUrl = apiUrl.replace("$" + listWatcher[i] + "%", $scope.model[listWatcher[i]]);
                                    }
                                    var selectPropResponse = $scope.to.apiResponseData;
                                    getOptionByApi(apiUrl, selectPropResponse);
                                }
                            });
                        } else {
                            var apiUrl = $scope.to.apiGetOption;
                            var selectPropResponse = $scope.to.apiResponseData;
                            getOptionByApi(apiUrl, selectPropResponse);
                        }

                    }
                }]
            });
            //savis-uiselect
            formlyConfig.setType({
                "name": "savis-uiselect",
                "wrapper": ["validation"],
                "defaultOptions": {
                    "className": "col-md-12",
                    "templateOptions": {
                        "labelSize": "20%",
                        "controlSize": "80%",
                        "required": true,
                        "disabled": false,
                        "placeholder": "Nhập vào text",
                        "label": "select",
                        "valueProp": "value",
                        "labelProp": "name",
                        "groupProp": "group",
                        "options": [{
                            value: "1",
                            name: "Option1"
                        }, {
                            value: "2",
                            name: "Option2"
                        }],
                        //Lấy các option từ api
                        "isGetOptionByApi": false,
                        "apiGetOption": "https://jsonplaceholder.typicode.com/comments?postId=$references",
                        "apiResponseData": '$response',


                        //hideExpression
                        "isUseHideExpression": false,
                        "hideExpression": "",
                        //hideExpression
                        "isUseDisabledExpression": false,
                        "disabledExpression": "",
                    },

                    "expressionProperties": {
                        "templateOptions.disabled": function ($viewValue, $modelValue, $scope) {
                            if ($scope.options.templateOptions.isUseDisabledExpression) {
                                var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.disabledExpression);
                                return customFunction($viewValue, $viewValue, $scope);
                            } else {
                                return false;
                            }
                        },
                        //"hide": function ($viewValue, $modelValue, $scope) {
                        //    if ($scope.options.templateOptions.isUseHideExpression) {
                        //        var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.hideExpression);
                        //        return customFunction($viewValue, $viewValue, $scope);
                        //    } else {
                        //        return false;
                        //    }
                        //},
                    },
                },
                "controller": ["$scope", "$timeout", function ($scope, $timeout) {
                    if (!Array.isArray($scope.options.wrapper)) {
                        $scope.options.wrapper = [];
                    }
                    if ($scope.options.templateOptions.horizontalLabel) {
                        var index = $scope.options.wrapper.indexOf("horizontalLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("horizontalLabel");
                    } else {
                        var index = $scope.options.wrapper.indexOf("bootstrapLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("bootstrapLabel");
                    }

                    //if (!$scope.options.templateOptions.required) {
                    //    $timeout(function () {
                    //        var defaultValue = {};
                    //        defaultValue[$scope.to.valueProp] = null;
                    //        defaultValue[$scope.to.labelProp] = "-- " + $scope.to.placeholder + " --";
                    //        $scope.to.options.unshift(defaultValue);
                    //        //$scope.model[$scope.options.key] = null;
                    //    });
                    //}
                    //hàm load động dữ liệu
                    var getOptionByApi = function (apiUrl, selectPropResponse) {
                        $scope.to.options = [];
                        var promise = $http({
                            method: 'GET',
                            url: apiUrl,
                        });
                        promise.success(function (response) {
                            $scope.to.options = [];
                            //if (!$scope.options.templateOptions.required) {
                            //    $timeout(function () {
                            //        var defaultValue = {};
                            //        defaultValue[$scope.to.valueProp] = null;
                            //        defaultValue[$scope.to.labelProp] = "-- " + $scope.to.placeholder + " --";
                            //        $scope.to.options.unshift(defaultValue);
                            //        //$scope.model[$scope.options.key] = null;
                            //    });
                            //}
                            if (selectPropResponse === "$response") {
                                if (Array.isArray(response)) {
                                    $scope.to.options = response;
                                    //if (!$scope.options.templateOptions.required) {
                                    //    $timeout(function () {
                                    //        var defaultValue = {};
                                    //        defaultValue[$scope.to.valueProp] = null;
                                    //        defaultValue[$scope.to.labelProp] = "-- " + $scope.to.placeholder + " --";
                                    //        $scope.to.options.unshift(defaultValue);
                                    //        //$scope.model[$scope.options.key] = null;
                                    //    });
                                    //}
                                }
                            } else {
                                var atribute = selectPropResponse.replace("$response.", "");
                                if (Array.isArray(response[atribute])) {
                                    $scope.to.options = response[atribute];
                                    //    if (!$scope.options.templateOptions.required) {
                                    //        $timeout(function () {
                                    //            var defaultValue = {};
                                    //            defaultValue[$scope.to.valueProp] = null;
                                    //            defaultValue[$scope.to.labelProp] = "-- " + $scope.to.placeholder + " --";
                                    //            $scope.to.options.unshift(defaultValue);
                                    //            //$scope.model[$scope.options.key] = null;
                                    //        });
                                    //    }
                                }
                            }


                        });
                        return promise;
                    };

                    if ($scope.to.isGetOptionByApi) {

                        //Solution  | watch all :)) but not this 
                        //lấy ra danh sách key
                        var re = /\$([^\%])+\%/g;
                        var s = $scope.to.apiGetOption;
                        var listWatcher = [];
                        var listWatcherModel = [];
                        var m;
                        do {
                            m = re.exec(s);
                            if (m) {
                                var key = m[0].replace("$", "");
                                key = key.replace("%", "");
                                listWatcher.push(key);
                                listWatcherModel.push("model." + key);
                            }
                        } while (m);

                        //end---lấy ra danh sách key
                        if (listWatcherModel.length > 0) {
                            var a = angular.toJson(listWatcherModel)
                            a = a.replace(/\"/g, '');
                            $scope.$watchCollection(a, function (newValue, oldValue) {
                                if (newValue !== oldValue) {
                                    var apiUrl = $scope.to.apiGetOption;
                                    if ($scope.model[$scope.options.key] && oldValue) {
                                        $scope.model[$scope.options.key] = '';
                                    }
                                    for (var i = 0; i < listWatcher.length; i++) {
                                        apiUrl = apiUrl.replace("$" + listWatcher[i] + "%", $scope.model[listWatcher[i]]);
                                    }
                                    var selectPropResponse = $scope.to.apiResponseData;
                                    getOptionByApi(apiUrl, selectPropResponse);
                                }
                            });
                        } else {
                            var apiUrl = $scope.to.apiGetOption;
                            var selectPropResponse = $scope.to.apiResponseData;
                            getOptionByApi(apiUrl, selectPropResponse);
                        }

                    }
                }]
            });
            //ui-select
            formlyConfig.setType({
                "name": "ui-select",
                "extends": "savis-uiselect",
                "templateUrl": "app-data/components/formly-template/control/ui-select.html",
                "wrapper": ["validation"],
                "defaultOptions": {
                    "templateOptions": {

                        "required": false,

                    },
                },
                "controller": ["$scope", "$timeout", function ($scope, $timeout) {
                    $scope.uiselect = {};
                    $scope.uiselect.OnChange = function (data) {
                        $scope.model[$scope.options.key + "-data"] = data;
                    };
                }]
            });
            //ui-select-tree
            formlyConfig.setType({
                "name": "ui-select-tree",
                "extends": "savis-uiselect",
                "templateUrl": "app-data/components/formly-template/control/ui-select-tree.html",
                "wrapper": ["validation"],
                "defaultOptions": {
                    "templateOptions": {
                        "required": false,
                    },
                },
                "controller": ["$scope", "$timeout", function ($scope, $timeout) {
                    //initData 
                    var checkData = function (listData, id) {
                        angular.forEach(listData, function (item) {
                            if (item[$scope.options.templateOptions.valueProp] == id) {
                                $scope.uiselect.SelectedValue = item[$scope.options.templateOptions.labelProp];
                            }
                            if (Array.isArray(item.SubChild) && item.SubChild.length >= 0) {
                                checkData(item.SubChild, id);
                            }
                        });
                    }
                    $scope.uiselect = {};
                    $scope.$watchCollection("options.templateOptions.options", function (newValue, oldValue) {
                        checkData($scope.options.templateOptions.options, $scope.model[$scope.options.key]);
                    });
                    $scope.uiselect.SetData = function (option) {
                        $scope.model[$scope.options.key + "-data"] = option;
                        $scope.model[$scope.options.key] = option[$scope.options.templateOptions.valueProp];
                        $scope.uiselect.SelectedValue = option[$scope.options.templateOptions.labelProp];
                        //$scope.uiselect.SearchValue = option[$scope.options.templateOptions.labelProp];
                        $scope.uiselect.IsOpen = false;

                    }
                    $scope.uiselect.OnClick = function () {
                        $scope.uiselect.IsOpen = !$scope.uiselect.IsOpen;

                    }
                    $scope.uiselect.OnClear = function () {
                        $scope.model[$scope.options.key + "-data"] = null;
                        $scope.model[$scope.options.key] = null;
                        $scope.uiselect.SelectedValue = null;
                        //$scope.uiselect.SearchValue = null;
                        $scope.uiselect.IsOpen = false;
                    }

                }]
            });
            //ui-select-multiple
            formlyConfig.setType({
                "name": "ui-select-multiple",
                "extends": "savis-uiselect",
                "templateUrl": "app-data/components/formly-template/control/ui-select-multiple.html",
                "wrapper": ["validation"],
                "defaultOptions": {
                    "templateOptions": {
                        "required": false,
                    }
                }, "controller": ["$scope", "$timeout", function ($scope, $timeout) {
                    $scope.uiselect = {};
                    $scope.uiselect.OnChange = function (data) {
                        $scope.model[$scope.options.key + "-data"] = data;
                    };
                }]
            });
            //Savis radioBtn
            formlyConfig.setType({
                "name": "savis-radioBtn",
                "extends": "radio",
                "templateUrl": "app-data/components/formly-template/control/radio.html",
                "wrapper": ["validation"],
                "defaultOptions": {
                    "className": "col-md-12",
                    "templateOptions": {
                        "labelSize": "20%",
                        "controlSize": "80%",
                        "required": true,
                        "disabled": false,
                        "placeholder": "Nhập vào text",
                        "label": "select",
                        "valueProp": "value",
                        "labelProp": "name",
                        "groupProp": "group",
                        "options": [{
                            value: "1",
                            name: "Option1"
                        }, {
                            value: "2",
                            name: "Option2"
                        }],
                        //Lấy các option từ api
                        "isGetOptionByApi": false,
                        "apiGetOption": "https://jsonplaceholder.typicode.com/comments?postId=$references",
                        "apiResponseData": '$response',


                        //hideExpression
                        "isUseHideExpression": false,
                        "hideExpression": "",
                        //hideExpression
                        "isUseDisabledExpression": false,
                        "disabledExpression": "",
                    },

                    "expressionProperties": {
                        "templateOptions.disabled": function ($viewValue, $modelValue, $scope) {
                            if ($scope.options.templateOptions.isUseDisabledExpression) {
                                var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.disabledExpression);
                                return customFunction($viewValue, $viewValue, $scope);
                            } else {
                                return false;
                            }
                        },
                        //"hide": function ($viewValue, $modelValue, $scope) {
                        //    if ($scope.options.templateOptions.isUseHideExpression) {
                        //        var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.hideExpression);
                        //        return customFunction($viewValue, $viewValue, $scope);
                        //    } else {
                        //        return false;
                        //    }
                        //},
                    },
                },
                "controller": ["$scope", "$timeout", function ($scope, $timeout) {
                    if (!Array.isArray($scope.options.wrapper)) {
                        $scope.options.wrapper = [];
                    }
                    if ($scope.options.templateOptions.horizontalLabel) {
                        var index = $scope.options.wrapper.indexOf("horizontalLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("horizontalLabel");
                    } else {
                        var index = $scope.options.wrapper.indexOf("bootstrapLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("bootstrapLabel");
                    }

                    //hàm load động dữ liệu
                    var getOptionByApi = function (apiUrl, selectPropResponse) {
                        $scope.to.options = [];
                        var promise = $http({
                            method: 'GET',
                            url: apiUrl,
                        });
                        promise.success(function (response) {
                            $scope.to.options = [];


                            if (selectPropResponse === "$response") {
                                if (Array.isArray(response)) {
                                    $scope.to.options = response;
                                }
                            } else {
                                var atribute = selectPropResponse.replace("$response.", "");
                                if (Array.isArray(response[atribute])) {
                                    $scope.to.options = response[atribute];
                                }
                            }
                        });
                        return promise;
                    };

                    if ($scope.to.isGetOptionByApi) {

                        //Solution  | watch all :)) but not this 
                        //lấy ra danh sách key
                        var re = /\$([^\%])+\%/g;
                        var s = $scope.to.apiGetOption;
                        var listWatcher = [];
                        var listWatcherModel = [];
                        var m;
                        do {
                            m = re.exec(s);
                            if (m) {
                                var key = m[0].replace("$", "");
                                key = key.replace("%", "");
                                listWatcher.push(key);
                                listWatcherModel.push("model." + key);
                            }
                        } while (m);

                        //end---lấy ra danh sách key
                        if (listWatcherModel.length > 0) {
                            var a = angular.toJson(listWatcherModel)
                            a = a.replace(/\"/g, '');
                            $scope.$watchCollection(a, function (newValue, oldValue) {
                                if (newValue !== oldValue) {
                                    var apiUrl = $scope.to.apiGetOption;
                                    if ($scope.model[$scope.options.key] && oldValue) {
                                        $scope.model[$scope.options.key] = '';
                                    }
                                    for (var i = 0; i < listWatcher.length; i++) {
                                        apiUrl = apiUrl.replace("$" + listWatcher[i] + "%", $scope.model[listWatcher[i]]);
                                    }
                                    var selectPropResponse = $scope.to.apiResponseData;
                                    getOptionByApi(apiUrl, selectPropResponse);
                                }
                            });
                        } else {
                            var apiUrl = $scope.to.apiGetOption;
                            var selectPropResponse = $scope.to.apiResponseData;
                            getOptionByApi(apiUrl, selectPropResponse);
                        }

                    }
                }]
            });
            //Savis checkbox-multi
            formlyConfig.setType({
                "name": "savis-checkboxMulti",
                "extends": "multiCheckbox",
                "templateUrl": "app-data/components/formly-template/control/check-box-multiple.html",
                "wrapper": ["validation"],
                "defaultOptions": {
                    "className": "col-md-12",
                    "templateOptions": {
                        "labelSize": "20%",
                        "controlSize": "80%",
                        "required": true,
                        "disabled": false,
                        "placeholder": "Nhập vào text",
                        "label": "select",
                        "valueProp": "value",
                        "labelProp": "name",
                        "groupProp": "group",
                        "options": [{
                            value: "1",
                            name: "Option1"
                        }, {
                            value: "2",
                            name: "Option2"
                        }],
                        //Lấy các option từ api
                        "isGetOptionByApi": false,
                        "apiGetOption": "https://jsonplaceholder.typicode.com/comments?postId=$references",
                        "apiResponseData": '$response',


                        //hideExpression
                        "isUseHideExpression": false,
                        "hideExpression": "",
                        //hideExpression
                        "isUseDisabledExpression": false,
                        "disabledExpression": "",
                    },

                    "expressionProperties": {
                        "templateOptions.disabled": function ($viewValue, $modelValue, $scope) {
                            if ($scope.options.templateOptions.isUseDisabledExpression) {
                                var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.disabledExpression);
                                return customFunction($viewValue, $viewValue, $scope);
                            } else {
                                return false;
                            }
                        },
                        //"hide": function ($viewValue, $modelValue, $scope) {
                        //    if ($scope.options.templateOptions.isUseHideExpression) {
                        //        var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.hideExpression);
                        //        return customFunction($viewValue, $viewValue, $scope);
                        //    } else {
                        //        return false;
                        //    }
                        //},
                    },
                },
                "controller": ["$scope", "$timeout", function ($scope, $timeout) {
                    if (!Array.isArray($scope.options.wrapper)) {
                        $scope.options.wrapper = [];
                    }
                    if ($scope.options.templateOptions.horizontalLabel) {
                        var index = $scope.options.wrapper.indexOf("horizontalLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("horizontalLabel");
                    } else {
                        var index = $scope.options.wrapper.indexOf("bootstrapLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("bootstrapLabel");
                    }

                    //hàm load động dữ liệu
                    var getOptionByApi = function (apiUrl, selectPropResponse) {
                        $scope.to.options = [];
                        var promise = $http({
                            method: 'GET',
                            url: apiUrl,
                        });
                        promise.success(function (response) {
                            $scope.to.options = [];


                            if (selectPropResponse === "$response") {
                                if (Array.isArray(response)) {
                                    $scope.to.options = response;
                                }
                            } else {
                                var atribute = selectPropResponse.replace("$response.", "");
                                if (Array.isArray(response[atribute])) {
                                    $scope.to.options = response[atribute];
                                }
                            }
                        });
                        return promise;
                    };

                    if ($scope.to.isGetOptionByApi) {

                        //Solution  | watch all :)) but not this 
                        //lấy ra danh sách key
                        var re = /\$([^\%])+\%/g;
                        var s = $scope.to.apiGetOption;
                        var listWatcher = [];
                        var listWatcherModel = [];
                        var m;
                        do {
                            m = re.exec(s);
                            if (m) {
                                var key = m[0].replace("$", "");
                                key = key.replace("%", "");
                                listWatcher.push(key);
                                listWatcherModel.push("model." + key);
                            }
                        } while (m);

                        //end---lấy ra danh sách key
                        if (listWatcherModel.length > 0) {
                            var a = angular.toJson(listWatcherModel)
                            a = a.replace(/\"/g, '');
                            $scope.$watchCollection(a, function (newValue, oldValue) {
                                if (newValue !== oldValue) {
                                    var apiUrl = $scope.to.apiGetOption;
                                    if ($scope.model[$scope.options.key] && oldValue) {
                                        $scope.model[$scope.options.key] = '';
                                    }
                                    for (var i = 0; i < listWatcher.length; i++) {
                                        apiUrl = apiUrl.replace("$" + listWatcher[i] + "%", $scope.model[listWatcher[i]]);
                                    }
                                    var selectPropResponse = $scope.to.apiResponseData;
                                    getOptionByApi(apiUrl, selectPropResponse);
                                }
                            });
                        } else {
                            var apiUrl = $scope.to.apiGetOption;
                            var selectPropResponse = $scope.to.apiResponseData;
                            getOptionByApi(apiUrl, selectPropResponse);
                        }

                    }
                }]
            });
            //Savis checkbox-multi-tree
            formlyConfig.setType({
                "name": "savis-checkboxMultiTree",
                "templateUrl": "app-data/components/formly-template/control/check-box-multiple-tree.html",
                "wrapper": ["validation"],
                "defaultOptions": {
                    "className": "col-md-12",
                    "templateOptions": {
                        "labelSize": "20%",
                        "controlSize": "80%",
                        "required": true,
                        "disabled": false,
                        "placeholder": "Nhập vào text",
                        "label": "select",
                        "valueProp": "value",
                        "labelProp": "name",
                        "groupProp": "group",
                        "options": [{
                            value: "1",
                            name: "Option1"
                        }, {
                            value: "2",
                            name: "Option2"
                        }],
                        //Lấy các option từ api
                        "isGetOptionByApi": false,
                        "apiGetOption": "https://jsonplaceholder.typicode.com/comments?postId=$references",
                        "apiResponseData": '$response',


                        //hideExpression
                        "isUseHideExpression": false,
                        "hideExpression": "",
                        //hideExpression
                        "isUseDisabledExpression": false,
                        "disabledExpression": "",
                    },

                    "expressionProperties": {
                        "templateOptions.disabled": function ($viewValue, $modelValue, $scope) {
                            if ($scope.options.templateOptions.isUseDisabledExpression) {
                                var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.disabledExpression);
                                return customFunction($viewValue, $viewValue, $scope);
                            } else {
                                return false;
                            }
                        },
                        //"hide": function ($viewValue, $modelValue, $scope) {
                        //    if ($scope.options.templateOptions.isUseHideExpression) {
                        //        var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.hideExpression);
                        //        return customFunction($viewValue, $viewValue, $scope);
                        //    } else {
                        //        return false;
                        //    }
                        //},
                    },
                },
                "controller": ["$scope", "$timeout", function ($scope, $timeout) {
                    $scope.multiCheckbox = {};
                    $scope.multiCheckbox.checked = {};
                    $scope.multiCheckbox.change = function (option) {
                        if ($scope.multiCheckbox.checked[option[$scope.to.valueProp]] === true) {
                            if (!Array.isArray($scope.model[$scope.options.key])) { $scope.model[$scope.options.key] = []; }
                            $scope.model[$scope.options.key].push(option[$scope.to.valueProp]);
                        } else {
                            var index = $scope.model[$scope.options.key].indexOf(option[$scope.to.valueProp]);
                            if (index >= 0) { $scope.model[$scope.options.key].splice(index, 1); }
                        }
                    }
                    //InitData
                    var checkData = function (listData, value) {
                        if (value !== null && typeof (value) !== "undefined") {
                            angular.forEach(listData, function (item) {
                                if (value.indexOf(item[$scope.options.templateOptions.valueProp]) >= 0) {
                                    $scope.multiCheckbox.checked[item[$scope.to.valueProp]] = true;
                                }
                                if (Array.isArray(item.SubChild) && item.SubChild.length >= 0) {
                                    checkData(item.SubChild, value);
                                }
                            });
                        }
                    }
                    $scope.$watchCollection("options.templateOptions.options", function (newValue, oldValue) {
                        checkData($scope.options.templateOptions.options, $scope.model[$scope.options.key]);
                    });

                    //angular.forEach($scope.model[$scope.options.key], function (item) {
                    //    multiCheckbox.checked[]

                    //});
                    ////////--//////////

                    if (!Array.isArray($scope.options.wrapper)) {
                        $scope.options.wrapper = [];
                    }
                    if ($scope.options.templateOptions.horizontalLabel) {
                        var index = $scope.options.wrapper.indexOf("horizontalLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("horizontalLabel");
                    } else {
                        var index = $scope.options.wrapper.indexOf("bootstrapLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("bootstrapLabel");
                    }

                    //hàm load động dữ liệu
                    var getOptionByApi = function (apiUrl, selectPropResponse) {
                        $scope.to.options = [];
                        var promise = $http({
                            method: 'GET',
                            url: apiUrl,
                        });
                        promise.success(function (response) {
                            $scope.to.options = [];


                            if (selectPropResponse === "$response") {
                                if (Array.isArray(response)) {
                                    $scope.to.options = response;
                                }
                            } else {
                                var atribute = selectPropResponse.replace("$response.", "");
                                if (Array.isArray(response[atribute])) {
                                    $scope.to.options = response[atribute];
                                }
                            }
                        });
                        return promise;
                    };

                    if ($scope.to.isGetOptionByApi) {
                        //Solution  | watch all :)) but not this 
                        //lấy ra danh sách key
                        var re = /\$([^\%])+\%/g;
                        var s = $scope.to.apiGetOption;
                        var listWatcher = [];
                        var listWatcherModel = [];
                        var m;
                        do {
                            m = re.exec(s);
                            if (m) {
                                var key = m[0].replace("$", "");
                                key = key.replace("%", "");
                                listWatcher.push(key);
                                listWatcherModel.push("model." + key);
                            }
                        } while (m);

                        //end---lấy ra danh sách key
                        if (listWatcherModel.length > 0) {
                            var a = angular.toJson(listWatcherModel)
                            a = a.replace(/\"/g, '');
                            $scope.$watchCollection(a, function (newValue, oldValue) {
                                if (newValue !== oldValue) {
                                    var apiUrl = $scope.to.apiGetOption;
                                    if ($scope.model[$scope.options.key] && oldValue) {
                                        $scope.model[$scope.options.key] = '';
                                    }
                                    for (var i = 0; i < listWatcher.length; i++) {
                                        apiUrl = apiUrl.replace("$" + listWatcher[i] + "%", $scope.model[listWatcher[i]]);
                                    }
                                    var selectPropResponse = $scope.to.apiResponseData;
                                    getOptionByApi(apiUrl, selectPropResponse);
                                }
                            });
                        } else {
                            var apiUrl = $scope.to.apiGetOption;
                            var selectPropResponse = $scope.to.apiResponseData;
                            getOptionByApi(apiUrl, selectPropResponse);
                        }

                    }
                }]
            });
            //Savis checkbox
            formlyConfig.setType({
                "name": "savis-checkbox",
                "extends": "checkbox",
                "templateUrl": "app-data/components/formly-template/control/check-box.html",
                "wrapper": ["validation"],
                "defaultOptions": {
                    "className": "col-md-12",
                    "templateOptions": {
                        "labelSize": "30%",
                        "controlSize": "70%",
                        "required": true,
                    },
                    "expressionProperties": {
                        "templateOptions.disabled": function ($viewValue, $modelValue, $scope) {
                            if ($scope.options.templateOptions.isUseDisabledExpression) {
                                var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.disabledExpression);
                                return customFunction($viewValue, $viewValue, $scope);
                            } else {
                                return false;
                            }
                        },
                        //"hide": function ($viewValue, $modelValue, $scope) {
                        //    if ($scope.options.templateOptions.isUseHideExpression) {
                        //        var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.hideExpression);
                        //        return customFunction($viewValue, $viewValue, $scope);
                        //    } else {
                        //        return false;
                        //    }
                        //},
                    },
                }
            });
            //uploadsingle
            formlyConfig.setType({
                "name": "savis-upload",
                "templateUrl": "app-data/components/formly-template/control/upload.html",
                "defaultOptions": {
                    "templateOptions": {
                        "apiUrl": "",
                        "returnProp": "data.result",
                        "optionalData": "",
                        "accept": "image/*,audio/*",
                        "maxsize": "2GB",
                        "uploadText": "Tải file lên",
                        "labelSize": "20%",
                        "controlSize": "80%",
                        "nameAtribute": "name",
                        "sizeAtribute": "size",
                        "multiUpload": true,
                    },
                    "expressionProperties": {
                        'templateOptions.disabled': function ($viewValue, $modelValue, $scope) {
                            if ($scope.options.templateOptions.isUseDisabledExpression) {
                                var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.disabledExpression);
                                return customFunction($viewValue, $viewValue, $scope);
                            } else {
                                return false;
                            }
                        },
                        //'hide': function ($viewValue, $modelValue, $scope) {
                        //    if ($scope.options.templateOptions.isUseHideExpression) {
                        //        var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.hideExpression);
                        //        return customFunction($viewValue, $viewValue, $scope);
                        //    } else {
                        //        return false;
                        //    }
                        //},
                    },


                },
                "controller": ["$scope", "$timeout", "Upload", function ($scope, $timeout, Upload) {
                    if (!Array.isArray($scope.options.wrapper)) {
                        $scope.options.wrapper = [];
                    }
                    if ($scope.options.templateOptions.horizontalLabel) {
                        var index = $scope.options.wrapper.indexOf("horizontalLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("horizontalLabel");
                    } else {
                        var index = $scope.options.wrapper.indexOf("bootstrapLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("bootstrapLabel");
                    }
                    $scope.upload = {};
                    $scope.upload.UploadControl = {};
                    $scope.upload.IsSelectAll = false;
                    $scope.upload.ListSelected = [];
                    //step1 init
                    $timeout(function () {
                        try {
                            $scope.upload.FileData = [];
                            if (typeof ($scope.model[$scope.options.key]) !== 'undefined' && $scope.model[$scope.options.key] !== null) {
                                $scope.upload.FileData = angular.fromJson($scope.model[$scope.options.key]);
                            }
                            console.log("e");
                        } catch (e) {
                            console.log(e);
                        }

                    }, 2000);
                    $scope.upload.RemoveAttachment = function (data) {
                        let index = $scope.upload.FileData.indexOf(data);
                        if (index >= 0) {
                            $scope.upload.FileData.splice(index, 1);
                            $scope.model[$scope.options.key] = angular.toJson($scope.upload.FileData);
                        }
                    }

                    $scope.upload.RemoveMultiAttachment = function (data) {
                        for (var i = 0; i < $scope.upload.ListSelected.length; i++) {
                            $scope.upload.RemoveAttachment($scope.upload.ListSelected[i]);
                            $scope.upload.IsSelectAll = false;
                        }
                        $scope.upload.ListSelected = [];
                    }


                    $scope.upload.ViewAttachment = function (data) {
                        var baseUrl = "http://210.245.26.130:8800/";
                        var prefixCoreApiUrl = "api/v1/core/nodes";
                        var link = baseUrl + prefixCoreApiUrl + "/" + data.NodeId + "/download";
                        window.open(link);
                    }
                    $scope.upload.IsUpLoadDone = true;
                    //AcessObject
                    function resolve(obj, path) {
                        path = path.split('.');
                        var current = obj;
                        while (path.length) {
                            if (typeof current !== 'object') return undefined;
                            current = current[path.shift()];
                        }
                        return current;
                    }
                    $scope.upload.SelectItem = function (item) {
                        if (!item.Selecting) {
                            var index = $scope.upload.ListSelected.indexOf(item);
                            if (index >= 0) {
                                $scope.upload.ListSelected.splice(index, 1);
                            }
                        } else {
                            $scope.upload.ListSelected.push(item);
                        }
                        if ($scope.upload.ListSelected.length === $scope.upload.FileData.length) {
                            $scope.upload.IsSelectAll = true;
                        } else {
                            $scope.upload.IsSelectAll = false;
                        }
                    }

                    $scope.upload.SelectAllItem = function () {
                        if ($scope.upload.ListSelected.length === $scope.upload.FileData.length) {
                            $scope.upload.ListSelected = [];
                            angular.forEach($scope.upload.FileData, function (file) {
                                file.Selecting = false;
                            });
                            $scope.upload.IsSelectAll = false;
                        } else {
                            $scope.upload.ListSelected = [];
                            angular.forEach($scope.upload.FileData, function (file) {
                                file.Selecting = true;
                                $scope.upload.ListSelected.push(file);
                            });
                            $scope.upload.IsSelectAll = true;
                        }
                    }
                    $scope.upload.UploadFiles = function (files, errFiles) {
                        $scope.upload.IsUpLoadDone = false;
                        $scope.upload.UploadControl.files = files;
                        $scope.upload.UploadControl.errFiles = errFiles;
                        var promise;
                        var apiUrl = "";
                        //if ($scope.to.apiUrl !== undefined || $scope.to.apiUrl !== null) {
                        //    apiUrl = $scope.to.apiUrl;
                        //}
                        var baseUrl = "http://210.245.26.130:8800/";
                        var prefixCoreApiUrl = "api/v1/core/nodes";
                        var appId = '48ed5b71-66dc-4725-9604-4c042e45fa3f';
                        var createdByUserName = 'savis_admin';
                        var userId = '7efa183f-0013-43d6-a972-29abd7a93bc1';
                        var isScanvirus = false;
                        var today = new Date();
                        var destinationPath = $scope.to.folderPath;
                        var authStr = "RUNNX0FVVEhfQVBJOnNlY3JldA==";
                        apiUrl = baseUrl + prefixCoreApiUrl + "/upload" +
                            '?AppId=' + appId +
                            '&UserId=' + userId +
                            '&CreateByUser=' + createdByUserName +
                            '&DestinationPath=' + destinationPath +
                            '&IsScanvirus=' + isScanvirus +
                            '&[[Authencation:' + authStr + ']]';

                        if ($scope.to.headerData !== undefined && $scope.to.headerData !== null) {
                            promise = Upload.upload({
                                url: apiUrl,
                                data: {
                                    file: files,
                                    data: $scope.to.bodyData
                                },
                                headers: $scope.to.headerData
                            });
                        } else {
                            promise = Upload.upload({
                                url: apiUrl,
                                data: {
                                    file: files,
                                    data: $scope.to.bodyData
                                },
                            });
                        }
                        promise.then(function (response) {
                            $scope.upload.IsUpLoadDone = true;
                            console.log(response);
                            var dataUse = resolve(response, $scope.to.returnProp);
                            if (dataUse !== undefined && dataUse !== null) {
                                if (!Array.isArray($scope.upload.FileData)) { $scope.upload.FileData = []; }

                                for (var i = 0; i < dataUse.length; i++) {
                                    let fileData = dataUse[i];
                                    if ($scope.to.multiUpload) {
                                        $scope.upload.FileData.push(fileData);
                                    } else {
                                        $scope.upload.FileData = [];
                                        $scope.upload.FileData.push(fileData);
                                    }
                                }
                                $scope.model[$scope.options.key] = angular.toJson($scope.upload.FileData);
                            }
                        }, function (response) {
                            console.log(response);
                        }, function (evt) {
                            $scope.upload.Progress = parseInt(100.0 * evt.loaded / evt.total);
                        });
                    }
                }]
            });
            //toggle
            formlyConfig.setType({
                "name": "toggle",
                "templateUrl": "app-data/components/formly-template/control/toggle.html",
                "wrapper": ["validation"],
                "defaultOptions": {
                    "templateOptions": {
                        trueLabel: "ON",
                        flaseLabel: "OFF",
                        horizontalLabel: true,
                    },
                    "expressionProperties": {
                        'templateOptions.disabled': function ($viewValue, $modelValue, $scope) {
                            if ($scope.options.templateOptions.isUseDisabledExpression) {
                                var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.disabledExpression);
                                return customFunction($viewValue, $viewValue, $scope);
                            } else {
                                return false;
                            }
                        },
                        //'hide': function ($viewValue, $modelValue, $scope) {
                        //    if ($scope.options.templateOptions.isUseHideExpression) {
                        //        var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.hideExpression);
                        //        return customFunction($viewValue, $viewValue, $scope);
                        //    } else {
                        //        return false;
                        //    }
                        //},
                    },
                },
                "controller": ["$scope", "$timeout", function ($scope, $timeout) {
                    if (!Array.isArray($scope.options.wrapper)) {
                        $scope.options.wrapper = [];
                    }
                    if ($scope.options.templateOptions.horizontalLabel) {
                        var index = $scope.options.wrapper.indexOf("horizontalLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("horizontalLabel");
                    } else {
                        var index = $scope.options.wrapper.indexOf("bootstrapLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("bootstrapLabel");
                    }
                }]
            });
            //Timepicker
            formlyConfig.setType({
                "name": "timepicker",
                "templateUrl": "app-data/components/formly-template/control/timepicker.html",
                "defaultOptions": {
                    "templateOptions": {
                    }
                },
                "controller": ["$scope", "$timeout", function ($scope, $timeout) {
                    if (!Array.isArray($scope.options.wrapper)) {
                        $scope.options.wrapper = [];
                    }
                    if ($scope.options.templateOptions.horizontalLabel) {
                        var index = $scope.options.wrapper.indexOf("horizontalLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("horizontalLabel");
                    } else {
                        var index = $scope.options.wrapper.indexOf("bootstrapLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("bootstrapLabel");
                    }
                }]
            });
            //Datepicker
            formlyConfig.setType({
                "name": "datepicker",
                "templateUrl": "app-data/components/formly-template/control/datepicker.html",
                "extends": "savis-input",
                "wrapper": [],
                "defaultOptions": {
                    "templateOptions": {
                        'showMode': false,
                        "datepickerOptions": {
                            "format": "",
                            'datepickerMode': "'month'",
                            'minMode': 'month',
                        }
                    }
                },
                "controller": ["$scope", "$timeout", function ($scope, $timeout) {
                    $scope.datepicker = {};
                    $scope.datepicker.opened = false;
                    $scope.datepicker.open = function ($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.datepicker.opened = !$scope.datepicker.opened;
                    };

                    $scope.datepicker.ChangeDateMode = function (mode) {
                        switch (mode) {
                            case 0:
                                $scope.options.templateOptions.datepickerOptions.minMode = 'day';
                                $scope.options.templateOptions.datepickerOptions.datepickerMode = "'day'";
                                $scope.options.templateOptions.datepickerOptions.format = 'dd/MM/yyyy';
                                $scope.model[$scope.options.key + "-TypeDateTime"] = 0;
                                break;
                            case 1:
                                $scope.options.templateOptions.datepickerOptions.minMode = 'month';
                                $scope.options.templateOptions.datepickerOptions.datepickerMode = "'month'";
                                $scope.options.templateOptions.datepickerOptions.format = 'MM/yyyy';
                                $scope.model[$scope.options.key + "-TypeDateTime"] = 1;
                                break;
                            case 2:
                                $scope.options.templateOptions.datepickerOptions.minMode = 'year';
                                $scope.options.templateOptions.datepickerOptions.datepickerMode = "'year'";
                                $scope.options.templateOptions.datepickerOptions.format = 'yyyy';
                                $scope.model[$scope.options.key + "-TypeDateTime"] = 2;
                                break;
                            default:
                                $scope.options.templateOptions.datepickerOptions.minMode = 'day';
                                $scope.options.templateOptions.datepickerOptions.datepickerMode = "'day'";
                                $scope.options.templateOptions.datepickerOptions.format = 'dd/MM/yyyy';
                                $scope.model[$scope.options.key + "-TypeDateTime"] = 0;
                                break;
                        }
                    };
                    //Bước init
                    $timeout(function () {
                        try {
                            if (typeof ($scope.model[$scope.options.key]) !== 'undefined' && $scope.model[$scope.options.key] !== null) {
                                var date = new Date(Date.parse($scope.model[$scope.options.key]));
                                date = new Date(date.getTime() - (60000 * date.getTimezoneOffset()));
                                $scope.model[$scope.options.key] = date;
                            }
                            if (typeof ($scope.model[$scope.options.key + "-TypeDateTime"]) !== 'undefined' && $scope.model[$scope.options.key + "-TypeDateTime"] !== null) {
                                //set default mode
                                //initData
                                $scope.datepicker.ChangeDateMode($scope.model[$scope.options.key + "-TypeDateTime"])
                            } else {
                                $scope.datepicker.ChangeDateMode(0);
                            }

                            console.log("e");
                        } catch (e) {
                            console.log(e);
                        }

                    }, 2000);


                }]
            });

            //Table ag-grid
            formlyConfig.setType({
                "name": "savis-ag-grid",
                "templateUrl": "app-data/components/formly-template/control/ag-grid.html",
                "defaultOptions": {
                    "templateOptions": {
                        "headerConfig": [{ "headerName": "Number", "type": "numberColumn", "field": "Number" }, { "headerName": "Text", "type": "textColumn", "field": "Text" }, { "headerName": "Date", "type": "dateColumn", "field": "Date" }],
                    },
                    "expressionProperties": {
                        'templateOptions.disabled': function ($viewValue, $modelValue, $scope) {
                            if ($scope.options.templateOptions.isUseDisabledExpression) {
                                var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.disabledExpression);
                                return customFunction($viewValue, $viewValue, $scope);
                            } else {
                                return false;
                            }
                        },
                        //'hide': function ($viewValue, $modelValue, $scope) {
                        //    if ($scope.options.templateOptions.isUseHideExpression) {
                        //        var customFunction = Function("$viewValue", "$modelValue", "$scope", $scope.options.templateOptions.hideExpression);
                        //        return customFunction($viewValue, $viewValue, $scope);
                        //    } else {
                        //        return false;
                        //    }
                        //},
                    },


                },
                "controller": ["$scope", "$timeout", "Upload", function ($scope, $timeout) {
                    //Config Label
                    if (!Array.isArray($scope.options.wrapper)) {
                        $scope.options.wrapper = [];
                    }
                    if ($scope.options.templateOptions.horizontalLabel) {
                        var index = $scope.options.wrapper.indexOf("horizontalLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("horizontalLabel");
                    } else {
                        var index = $scope.options.wrapper.indexOf("bootstrapLabel");
                        if (index >= 0) { $scope.options.wrapper.splice(index, 1); }
                        $scope.options.wrapper.push("bootstrapLabel");
                    }
                    /*Common fuction--------------*/
                    var render = {};
                    render.Handler = function (params) {
                        var rowindex = params.rowIndex;
                        params.$scope.deleteRow = $scope.table.DeleteRow;
                        params.$scope.editRow = $scope.table.EditRow;
                        return '<button type="button" class="btn btn-xs btn-primary" ng-click="editRow(' + rowindex + ',data)"><i class="fa fa-edit"></i></button>' +
                            '&nbsp;&nbsp;<button type="button" class="btn btn-xs btn-danger" ng-click="deleteRow(' + rowindex + ',data)"><i class="fa fa-trash"></i></button>';
                    };
                    var getter = {};
                    getter.Index = function (params) {
                        return params.node.childIndex + 1;
                    };
                    var newGuid = function () {
                        function s4() {
                            return Math.floor((1 + Math.random()) * 0x10000)
                                .toString(16)
                                .substring(1);
                        }
                        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                            s4() + '-' + s4() + s4() + s4();
                    };
                    var saveModel = function () {
                        var rowData = [];
                        $scope.table.gridOptions.api.forEachNode(function (node) {
                            rowData.push(node.data);
                        });
                        $scope.model[$scope.options.key] = rowData;
                    }
                    /*NumericCellEditor fuction--------------*/
                    // function to act as a class
                    function NumericCellEditor() { }
                    // gets called once before the renderer is used
                    NumericCellEditor.prototype.init = function (params) {
                        // we only want to highlight this cell if it started the edit, it is possible
                        // another cell in this row started teh edit
                        this.focusAfterAttached = params.cellStartedEdit;
                        // create the cell
                        this.eInput = document.createElement('input');
                        this.eInput.style.width = '100%';
                        this.eInput.style.height = '100%';
                        this.eInput.value = isCharNumeric(params.charPress) ? params.charPress : params.value;
                        var that = this;
                        this.eInput.addEventListener('keypress', function (event) {
                            if (!isKeyPressedNumeric(event)) {
                                that.eInput.focus();
                                if (event.preventDefault) event.preventDefault();
                            }
                        });
                    };
                    // gets called once when grid ready to insert the element
                    NumericCellEditor.prototype.getGui = function () {
                        return this.eInput;
                    };
                    // focus and select can be done after the gui is attached
                    NumericCellEditor.prototype.afterGuiAttached = function () {
                        // only focus after attached if this cell started the edit
                        if (this.focusAfterAttached) {
                            this.eInput.focus();
                            this.eInput.select();
                        }
                    };
                    // returns the new value after editing
                    NumericCellEditor.prototype.isCancelBeforeStart = function () {
                        return this.cancelBeforeStart;
                    };
                    // example - will reject the number if it contains the value 007
                    // - not very practical, but demonstrates the method.
                    NumericCellEditor.prototype.isCancelAfterEnd = function () {
                    };
                    // returns the new value after editing
                    NumericCellEditor.prototype.getValue = function () {
                        return this.eInput.value;
                    };
                    // when we tab onto this editor, we want to focus the contents
                    NumericCellEditor.prototype.focusIn = function () {
                        var eInput = this.getGui();
                        eInput.focus();
                        eInput.select();
                        console.log('NumericCellEditor.focusIn()');
                    };
                    // when we tab out of the editor, this gets called
                    NumericCellEditor.prototype.focusOut = function () {
                        // but we don't care, we just want to print it for demo purposes
                        console.log('NumericCellEditor.focusOut()');
                    };
                    function isCharNumeric(charStr) {
                        return !!/\d/.test(charStr);
                    }
                    function isKeyPressedNumeric(event) {
                        var charCode = getCharCodeFromEvent(event);
                        var charStr = String.fromCharCode(charCode);
                        return isCharNumeric(charStr);
                    }
                    function getCharCodeFromEvent(event) {
                        event = event || window.event;
                        return (typeof event.which === "undefined") ? event.keyCode : event.which;
                    }
                    /*Datepicker fuction--------------*/
                    // function to act as a class
                    function Datepicker() { }
                    // gets called once before the renderer is used
                    Datepicker.prototype.init = function (params) {
                        // create the cell
                        this.eInput = document.createElement('input');
                        this.eInput.value = params.value;

                        // https://jqueryui.com/datepicker/
                        $(this.eInput).datepicker({
                            dateFormat: "dd/mm/yy"
                        });
                    };

                    // gets called once when grid ready to insert the element
                    Datepicker.prototype.getGui = function () {
                        return this.eInput;
                    };

                    // focus and select can be done after the gui is attached
                    Datepicker.prototype.afterGuiAttached = function () {
                        this.eInput.focus();
                        this.eInput.select();
                    };

                    // returns the new value after editing
                    Datepicker.prototype.getValue = function () {
                        return this.eInput.value;
                    };

                    // any cleanup we need to be done here
                    Datepicker.prototype.destroy = function () {
                        // but this example is simple, no cleanup, we could
                        // even leave this method out as it's optional
                    };

                    // if true, then this editor will appear in a popup
                    Datepicker.prototype.isPopup = function () {
                        // and we could leave this method out also, false is the default
                        return false;
                    };
                    //Step 1 setHeader
                    var columnDefs = [];
                    try {
                        columnDefs = $scope.to.headerConfig;
                        columnDefs.unshift({
                            headerName: "STT", headerClass: '',
                            pinned: 'left', valueGetter: getter.Index, suppressMenu: true, suppressSorting: true, suppressCount: true, type: "nonEditableColumn",
                        });
                        columnDefs.unshift({
                            headerName: "", headerClass: '',
                            pinned: 'left', suppressMenu: true, suppressSorting: true, suppressCount: true, width: 14,
                            headerCheckboxSelection: true,
                            headerCheckboxSelectionFilteredOnly: true,
                            checkboxSelection: true
                        });
                        columnDefs.push({
                            headerName: "Thao tác", headerClass: '', cellRenderer: render.Handler,
                            pinned: 'right', suppressMenu: true, suppressSorting: true, suppressCount: true, width: 45,
                        });
                    } catch (e) {
                        columnDefs = [];
                    }
                    $scope.table = {};
                    //Step2 setGrid
                    $scope.table.gridOptions = {
                        //Phục vụ angularjs
                        angularCompileRows: true,
                        //Row động
                        animateRows: true,
                        // cấu hình cột
                        columnDefs: columnDefs,
                        //edit type
                        editType: 'fullRow',
                        // dữ liệu vào
                        rowData: null,
                        //Cho phép resize col 
                        enableColResize: true,
                        // Sử dụng filter
                        enableFilter: true,
                        // Sử dụng filter float
                        //floatingFilter: true,
                        // Sử dụng sort
                        enableSorting: true,
                        sortingOrder: ['desc', 'asc', null],
                        accentedSort: true,
                        //Cho phép chọn
                        rowSelection: 'multiple',
                        enableRangeSelection: true,
                        //deta
                        //deltaRowDataMode: true,
                        //enableCellChangeFlash: true,
                        //getRowNodeId: function (data) { return data.Id },
                        //Event
                        onRowEditingStarted: function (event) {
                            $scope.table.IsEditing = true;
                        },
                        onRowEditingStopped: function (event) {
                            $scope.table.IsEditing = false;
                            saveModel();
                        },
                        onSelectionChanged: function (event) {
                            $scope.table.SelectedCount = $scope.table.gridOptions.api.getSelectedNodes().length;
                        },
                        onGridReady: function (params) {
                            params.columnApi.autoSizeAllColumns();
                            //params.api.sizeColumnsToFit();
                            $timeout(function () {
                                $('.ag-cell').each(function (index, value) {
                                    //$(this).attr('data-html', true);
                                    //$(this).attr('title', $(this).html());
                                    //$(this).tooltip();
                                    $(this).attr('title', $(this).text());
                                });
                            }, 3000);
                        },
                        // define specific column types
                        columnTypes: {
                            "nonEditableColumn": { editable: false, width: 90 },
                            "textColumn": { editable: true, filter: 'text', },
                            "numberColumn": { editable: true, filter: 'number', width: 100, cellEditor: NumericCellEditor },
                            "dateColumn": {
                                cellEditor: Datepicker,
                                editable: true,
                                filter: 'date',
                                filterParams: {
                                    comparator: function (filterLocalDateAtMidnight, cellValue) {
                                        // In the example application, dates are stored as dd/mm/yyyy
                                        // We create a Date object for comparison against the filter date
                                        var dateParts = cellValue.split("/");
                                        var day = Number(dateParts[2]);
                                        var month = Number(dateParts[1]) - 1;
                                        var year = Number(dateParts[0]);
                                        var cellDate = new Date(day, month, year);
                                        // Now that both parameters are Date objects, we can compare
                                        if (cellDate < filterLocalDateAtMidnight) {
                                            return -1;
                                        } else if (cellDate > filterLocalDateAtMidnight) {
                                            return 1;
                                        } else {
                                            return 0;
                                        }
                                    }
                                }
                            },
                        },
                        overlayLoadingTemplate:
                        '<div class="bookshelf_wrapper default-color">' +
                        '<ul class="books_list" >' +
                        '<li class="book_item first"></li>' +
                        '<li class="book_item second"></li>' +
                        '<li class="book_item third"></li>' +
                        '<li class="book_item fourth"></li>' +
                        '<li class="book_item fifth"></li>' +
                        '<li class="book_item sixth"></li>' +
                        '</ul>' +
                        '<div class="shelf" ></div>' +
                        '</div>',
                        overlayNoRowsTemplate: '<span style="padding: 10px; border: 2px solid #444; background: lightgoldenrodyellow;">Không có dữ liệu</span>',
                        //domLayout: 'autoHeight',
                        localeText: {
                            // loadingOoo: 'Đang tải...',
                            // searchOoo: 'Tìm kiếm...',
                            // filterOoo: 'Lọc...',
                            // applyFilter: 'Áp dụng lọc',
                            // equals: 'bằng',
                            // notEqual: 'không bằng',
                            // lessThanOrEqual: 'nhỏ hơn hoặc bằng',
                            // greaterThanOrEqual: 'lớn hơn hoặc bằng',
                            // inRange: 'trong khoảng',
                            // lessThan: 'nhỏ hơn',
                            // greaterThan: 'lớn hơn',
                            // contains: 'chứa',
                            // notContains: 'không chứa',
                            // startsWith: 'bắt đầu bằng',
                            // endsWith: 'kết thúc bằng',
                            // group: 'nhóm',
                            // noRowsToShow: 'chưa có dữ liệu',
                            // copy: 'sao chép',
                            // ctrlC: 'ctrl + C',
                            // paste: 'dán',
                            // ctrlV: 'ctrl + V'
                        },
                    };
                    $scope.table.DeleteSelected = function () {
                        var selectedData = $scope.table.gridOptions.api.getSelectedRows();
                        var res = $scope.table.gridOptions.api.updateRowData({ remove: selectedData });
                        saveModel();
                    }
                    $scope.table.RevertEdit = function () {
                        $scope.table.gridOptions.api.stopEditing(true);
                    }
                    $scope.table.SaveEdit = function () {
                        $scope.table.gridOptions.api.stopEditing();
                    }

                    $scope.table.AddRow = function () {
                        var newItem = {
                            "Id": newGuid(),
                        };
                        var res = $scope.table.gridOptions.api.updateRowData({ add: [newItem] });
                        saveModel();
                        var index = $scope.model[$scope.options.key].length - 1;
                        $scope.table.gridOptions.api.setFocusedCell(index, columnDefs[2].field);
                        $scope.table.gridOptions.api.startEditingCell({
                            rowIndex: index,
                            colKey: columnDefs[2].field,
                        });
                    }
                    $scope.table.DeleteRow = function (index, data) {
                        var res = $scope.table.gridOptions.api.updateRowData({ remove: [data] });
                    }
                    $scope.table.EditRow = function (index) {
                        $scope.table.gridOptions.api.setFocusedCell(index, columnDefs[2].field);
                        $scope.table.gridOptions.api.startEditingCell({
                            rowIndex: index,
                            colKey: columnDefs[2].field,
                        });
                    }



                    //step3 init
                    $timeout(function () {
                        if (!Array.isArray($scope.model[$scope.options.key])) {
                            $scope.model[$scope.options.key] = [];
                        }
                        $scope.table.gridOptions.api.setRowData($scope.model[$scope.options.key]);
                    }, 2000);
                }]
            });
        });

        //DatePicker
        app.run(function (formlyConfig) {


        });
    };

    return factory;
});
