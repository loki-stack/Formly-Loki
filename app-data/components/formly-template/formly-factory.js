﻿'use strict';
define(['app'], function (app) {
    app.service('FormlyFactory', function () {
        var factory = {};

        // Return factory
        //Input
        factory.InputTextControl = {
            "data": {
                "type": 0,
                "name": "Input text",
                "key": "InputTextControl",
                "group": "input",

                "availableDbType": [
                    { Name: "Text Data UTF8", Value: "NvarcharValue" }
                ],
                "dbType": "NvarcharValue",
                "dbtypeName": "Text Data UTF8",
            },
            "type": "savis-input",
            "key": "InputTextControl",

            "className": "formly-control",
            "wrapper": ["validation", "loader"],
            "templateOptions": {
                "type": "text",
                "label": "InputTextControl",
                "required": false,
                "disabled": false,
                "placeholder": "Input text",
                "isValidateByApi": false,
                "apiValidate": "%",
                "apiValidateCondition": "if($response){return true;}else{return false;}",
                "apiValidateError": "Error Api Validate",
                "isValidateByParttern": false,
                "partternValidate": "",
                "partternValidateError": "Error Parttern Validate",
                "isValidateByCustom": false,
                "customValidate": "",
                "customValidateError": "Error Custom Validate",
                "isUseHideExpression": false,
                "hideExpression": "",
                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            },
            "modelOptions": {
                "updateOn": 'default',
            },
        };
        factory.InputEmailControl = {
            "data": {
                "type": 0,
                "name": "Input email",
                "key": "InputEmailControl",
                "group": "input",
                "availableDbType": [
                    { Name: "Text Data UTF8", Value: "NvarcharValue" }],
                "dbType": "NvarcharValue",
                "dbtypeName": "Text Data UTF8",
            },
            "type": "savis-input",
            "wrapper": ["validation", "loader"],
            "key": "InputEmailControl",

            "className": "formly-control",
            "templateOptions": {
                "type": "text",
                "label": "InputEmailControl",
                "required": false,
                "disabled": false,
                "placeholder": "Input email",
                "addonRight": {
                    "class": "fa fa-envelope"
                },
                "isValidateByApi": false,
                "apiValidate": "",
                "apiValidateCondition": "if($response){return true;}else{return false;}",
                "apiValidateError": "Error Api Validate",
                "isValidateByParttern": true,
                "partternValidate": "^(([^<>()\\[\\]\\.,;:\\s@\\\"]+(\\.[^<>()\\[\\]\\.,;:\\s@\\\"]+)*)|(\\\".+\\\"))@(([^<>()[\\]\\.,;:\\s@\\\"]+\\.)+[^<>()[\\]\\.,;:\\s@\\\"]{2,})$",
                "partternValidateError": "email invalid",
                "isValidateByCustom": false,
                "customValidate": "",
                "customValidateError": "Error Custom Validate",
                "isUseHideExpression": false,
                "hideExpression": "",
                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            },
            "modelOptions": {
                "updateOn": 'default',
            },
        };
        factory.InputUrlControl = {
            "data": {
                "type": 0,
                "name": "Input url",
                "key": "InputUrlControl",
                "group": "input",
                "availableDbType": [
                    { Name: "Text Data UTF8", Value: "NvarcharValue" }
                ],
                "dbType": "NvarcharValue",
                "dbtypeName": "Text Data UTF8",
            },
            "type": "savis-input",
            "wrapper": ["validation", "loader"],
            "key": "InputUrlControl",

            "className": "formly-control",
            "templateOptions": {
                "type": "text",
                "label": "InputUrlControl",
                "required": false,
                "disabled": false,
                "placeholder": "Input url",
                "addonRight": {
                    "class": "fa fa-link"
                },
                "isValidateByApi": false,
                "apiValidate": "",
                "apiValidateCondition": "if($response){return true;}else{return false;}",
                "apiValidateError": "Error Api Validate",
                "isValidateByParttern": true,
                "partternValidate": "[-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%_\\+.~#?&//=]*)",
                "partternValidateError": "url invalid",
                "isValidateByCustom": false,
                "customValidate": "",
                "customValidateError": "Error Custom Validate",
                "isUseHideExpression": false,
                "hideExpression": "",
                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            },
            "modelOptions": {
                "updateOn": 'default',
            },
        };
        factory.InputCodeControl = {
            "data": {
                "type": 0,
                "name": "Input code",
                "key": "InputCodeControl",
                "group": "input",

                "availableDbType": [
                    { Name: "Text Data", Value: "VarcharValue" }
                ],
                "dbType": "VarcharValue",
                "dbtypeName": "Text Data",
            },
            "type": "savis-input",
            "wrapper": ["validation", "loader"],
            "key": "InputCodeControl",

            "className": "formly-control",
            "templateOptions": {
                "type": "text",
                "label": "InputCodeControl",
                "required": false,
                "disabled": false,
                "placeholder": "Input code",
                "addonRight": {
                    "class": "fa fa-barcode"
                },
                "isValidateByApi": false,
                "apiValidate": "",
                "apiValidateCondition": "if($response){return true;}else{return false;}",
                "apiValidateError": "Error Api Validate",
                "isValidateByParttern": true,
                "partternValidate": "^([\x00-\x7F]|[\xC2-\xDF][\x80-\xBF]|\xE0[\xA0-\xBF][\x80-\xBF]|[\xE1-\xEC][\x80-\xBF]{2}|\xED[\x80-\x9F][\x80-\xBF]|[\xEE-\xEF][\x80-\xBF]{2}|\xF0[\x90-\xBF][\x80-\xBF]{2}|[\xF1-\xF3][\x80-\xBF]{3}|\xF4[\x80-\x8F][\x80-\xBF]{2})*$",
                "partternValidateError": "code invalid",
                "isValidateByCustom": false,
                "customValidate": "",
                "customValidateError": "Error Custom Validate",
                "isUseHideExpression": false,
                "hideExpression": "",
                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            },
            "modelOptions": {
                "updateOn": 'default',
            },
        };
        factory.InputPasswordControl = {
            "data": {
                "type": 0,
                "name": "Input password",
                "key": "InputPasswordControl",
                "group": "input",

                "availableDbType": [
                    { Name: "Text Data UTF8", Value: "NvarcharValue" },
                    { Name: "Text Data", Value: "VarcharValue" }],
                "dbType": "NvarcharValue",
                "dbtypeName": "Text Data UTF8",
            },
            "type": "savis-input",
            "wrapper": ["validation", "loader"],
            "key": "InputPasswordControl",

            "className": "formly-control",
            "templateOptions": {
                "type": "password",
                "label": "InputPasswordControl",
                "required": false,
                "disabled": false,
                "placeholder": "Input password",

                "isValidateByApi": false,
                "apiValidate": "",
                "apiValidateCondition": "if($response){return true;}else{return false;}",
                "apiValidateError": "Error Api Validate",


                "isValidateByParttern": false,
                "partternValidate": "",
                "partternValidateError": "Error Parttern Validate",

                "isValidateByCustom": false,
                "customValidate": "",
                "customValidateError": "Error Custom Validate",

                "isUseHideExpression": false,
                "hideExpression": "",

                "isUseDisabledExpression": false,
                "disabledExpression": "",


                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            },
        };

        factory.InputNumberControl = {
            "data": {
                "type": 0,
                "name": "Input number",
                "key": "InputNumberControl",
                "group": "input",
                "availableDbType": [{ Name: "Number data", Value: "IntValue" }],
                "dbType": "IntValue",
                "dbtypeName": "Number data",
            },
            "type": "savis-input",
            "wrapper": ["validation", "loader"],
            "key": "InputNumberControl",
            "defaultValue": 0,
            "className": "formly-control",
            "templateOptions": {
                "type": "number",
                "label": "InputNumberControl",
                "required": false,
                "disabled": false,
                "min": null,
                "max": null,
                "step": 1,
                "placeholder": "Input number",
                "isValidateByParttern": false,
                "partternValidate": "",
                "partternValidateError": "Error Parttern Validate",
                "isValidateByCustom": false,
                "customValidate": "",
                "customValidateError": "Error Custom Validate",
                "isUseHideExpression": false,
                "hideExpression": "",
                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            },
            "modelOptions": {
                "updateOn": 'default',
            },
        };
        factory.InputRangeControl = {
            "data": {
                "type": 0,
                "name": "Input range",
                "key": "InputRangeControl",
                "group": "input",
                "availableDbType": [{ Name: "Number data", Value: "IntValue" }],
                "dbType": "IntValue",
                "dbtypeName": "Number data",
            },
            "type": "savis-input",
            "wrapper": ["validation", "loader", "range"],
            "key": "range-Input",
            "defaultValue": 0,
            "className": "formly-control",
            "templateOptions": {
                "type": "range",
                "label": "InputRangeControl",
                "required": false,
                "disabled": false,
                "min": 0,
                "max": 100,
                "step": 1,
                "isValidateByParttern": false,
                "partternValidate": "",
                "partternValidateError": "Error Parttern Validate",
                "isValidateByCustom": false,
                "customValidate": "",
                "customValidateError": "Error Custom Validate",
                "isUseHideExpression": false,
                "hideExpression": "",
                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            },
            "modelOptions": {
                "updateOn": 'default',
            },
        };

        factory.InputDateFullControl = {
            "data": {
                "type": 0,
                "name": "Input DatePicker",
                "key": "InputDateControl",
                "group": "input",
                "availableDbType": [{ Name: "Date time data", Value: "DateValue" }],
                "dbType": "DateValue",
                "dbtypeName": "Date time data",
            },
            "className": "formly-control",

            "key": 'datepicker',
            "type": 'datepicker',
            "wrapper": ["validation", "loader"],
            "templateOptions": {
                "label": 'InputDateControl',
                "type": 'text',
                "datepickerPopup": 'dd/MM/yyyy',
                "datepickerOptions": {
                    "format": 'dd/MM/yyyy',
                    "minMode": 'day',
                },
                'showMode': true,
                "isValidateByApi": false,
                "apiValidate": "",
                "apiValidateCondition": "if($response){return true;}else{return false;}",
                "apiValidateError": "Error Api Validate",
                "isValidateByParttern": false,
                "partternValidate": "",
                "partternValidateError": "Error Parttern Validate",
                "isValidateByCustom": false,
                "customValidate": "",
                "customValidateError": "Error Custom Validate",
                "isUseHideExpression": false,
                "hideExpression": "",
                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            }
        };
        factory.InputDateControl = {
            "data": {
                "type": 0,
                "name": "Input DatePicker",
                "key": "InputDateControl",
                "group": "date",
                "availableDbType": [{ Name: "Date time data", Value: "DateValue" }],
                "dbType": "DateValue",
                "dbtypeName": "Date time data",
            },
            "className": "formly-control",

            "key": 'datepicker',
            "type": 'datepicker',
            "wrapper": ["validation", "loader"],
            "templateOptions": {
                "label": 'InputDateControl',
                "type": 'text',
                "datepickerPopup": 'dd/MM/yyyy',
                "datepickerOptions": {
                    "format": 'dd/MM/yyyy',
                    "minMode": 'day',
                },
                "isValidateByApi": false,
                "apiValidate": "",
                "apiValidateCondition": "if($response){return true;}else{return false;}",
                "apiValidateError": "Error Api Validate",
                "isValidateByParttern": false,
                "partternValidate": "",
                "partternValidateError": "Error Parttern Validate",
                "isValidateByCustom": false,
                "customValidate": "",
                "customValidateError": "Error Custom Validate",
                "isUseHideExpression": false,
                "hideExpression": "",
                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            }
        };
        factory.InputMonthControl = {
            "data": {
                "type": 0,
                "name": "Input DatePicker",
                "key": "InputMonthControl",
                "group": "date",
                "availableDbType": [{ Name: "Kiểu tháng", Value: "MonthValue" }],
                "dbType": "MonthValue",
                "dbtypeName": "Kiểu tháng",
            },
            "className": "formly-control",

            "key": 'datepicker',
            "type": 'datepicker',
            "wrapper": ["validation", "loader"],
            "templateOptions": {
                "label": 'InputMonthControl',
                "type": 'text',
                "datepickerPopup": 'MM/yyyy',
                "datepickerOptions": {
                    "format": 'MM/yyyy',
                    "minMode": 'month',
                },
                "isValidateByApi": false,
                "apiValidate": "",
                "apiValidateCondition": "if($response){return true;}else{return false;}",
                "apiValidateError": "Error Api Validate",
                "isValidateByParttern": false,
                "partternValidate": "",
                "partternValidateError": "Error Parttern Validate",
                "isValidateByCustom": false,
                "customValidate": "",
                "customValidateError": "Error Custom Validate",
                "isUseHideExpression": false,
                "hideExpression": "",
                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            }
        };
        factory.InputYearControl = {
            "data": {
                "type": 0,
                "name": "InputYearControl",
                "key": "InputYearControl",
                "group": "date",
                "availableDbType": [{ Name: "Year data", Value: "YearValue" }],
                "dbType": "YearValue",
                "dbtypeName": "Year data",
            },
            "className": "formly-control",

            "key": 'datepicker',
            "type": 'datepicker',
            "wrapper": ["validation", "loader"],
            "templateOptions": {
                "label": 'InputYearControl',
                "type": 'text',
                "datepickerPopup": 'yyyy',
                "datepickerOptions": {
                    "format": 'yyyy',
                    "minMode": 'year',
                },
                "isValidateByApi": false,
                "apiValidate": "",
                "apiValidateCondition": "if($response){return true;}else{return false;}",
                "apiValidateError": "Error Api Validate",
                "isValidateByParttern": false,
                "partternValidate": "",
                "partternValidateError": "Error Parttern Validate",
                "isValidateByCustom": false,
                "customValidate": "",
                "customValidateError": "Error Custom Validate",
                "isUseHideExpression": false,
                "hideExpression": "",
                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            }
        };
        factory.InputTimeControl = {
            "data": {
                "type": 0,
                "name": "InputTimeControl ",
                "key": "InputTimeControl",
                "group": "date",
                "availableDbType": [{ Name: "Time data", Value: "TimeValue" }],
                "dbType": "TimeValue",
                "dbtypeName": "Time data",
            },
            "className": "formly-control",

            "key": 'timepicker',
            "type": 'timepicker',
            "wrapper": ["validation", "loader"],
            "templateOptions": {
                "label": 'InputTimeControl',
                "type": 'text',
                "isValidateByApi": false,
                "apiValidate": "",
                "apiValidateCondition": "if($response){return true;}else{return false;}",
                "apiValidateError": "Error Api Validate",
                "isValidateByParttern": false,
                "partternValidate": "",
                "partternValidateError": "Error Parttern Validate",
                "isValidateByCustom": false,
                "customValidate": "",
                "customValidateError": "Error Custom Validate",
                "isUseHideExpression": false,
                "hideExpression": "",
                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            }
        };

        factory.InputColorControl = {
            "data": {
                "type": 0,
                "name": "Input color",
                "key": "InputColorControl",
                "group": "color",

                "availableDbType": [
                    { Name: "Text Data UTF8", Value: "NvarcharValue" },
                    { Name: "Text Data", Value: "VarcharValue" }],
                "dbType": "NvarcharValue",
                "dbtypeName": "Text Data UTF8",
            },
            "type": "savis-input",
            "wrapper": ["color"],
            "key": "InputColorControl",

            "className": "formly-control",
            "templateOptions": {
                "type": "color",
                "label": "InputColorControl",
                "required": false,
                "disabled": false,

                "isUseHideExpression": false,
                "hideExpression": "",

                "isUseDisabledExpression": false,
                "disabledExpression": "",

                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            },
            "modelOptions": {
                "updateOn": 'default',
            },

        };


        factory.TextAreaControl = {
            "data": {
                "type": 0,
                "name": "Input textarea",
                "key": "TextAreaControl",

                "availableDbType": [
                    { Name: "Text Data UTF8", Value: "NvarcharValue" },
                    { Name: "Text Data", Value: "VarcharValue" }],
                "dbType": "NvarcharValue",
                "dbtypeName": "Text Data UTF8",
            },
            "type": "savis-textarea",
            "wrapper": [],

            "key": "textAreaControl",
            "className": "formly-control-big",
            "templateOptions": {
                "placeholder": "",
                "label": "TextAreaControl",
                "line": 5,
                //hideExpression
                "isUseHideExpression": false, //lựa chọn validate custom
                "hideExpression": "", //câu điều kiện
                //hideExpression
                "isUseDisabledExpression": false, //lựa chọn validate custom
                "disabledExpression": "", //câu điều kiện
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            }
        };
        factory.CkeditorControl = {
            "data": {
                "type": 0,
                "name": "CK-editor",
                "key": "CkeditorControl",

                "availableDbType": [
                    { Name: "Text Data UTF8", Value: "NvarcharValue" },
                    { Name: "Text Data", Value: "VarcharValue" }],
                "dbType": "NvarcharValue",
                "dbtypeName": "Text Data UTF8",
            },
            "type": "ckeditor",
            "wrapper": [],

            "key": "ckeditorControl",
            "className": "formly-control-big",
            "templateOptions": {
                "placeholder": "Input textarea",
                "label": "CkeditorControl",
                "line": 5,
                "isUseHideExpression": false,
                "hideExpression": "",

                "isUseDisabledExpression": false,
                "disabledExpression": "",

                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            }
        };

        factory.UploadControl = {
            "data": {
                "type": 0,
                "name": "Upload",
                "key": "UploadControl",
                "group": "upload",

                "availableDbType": [
                    { Name: "Text Data UTF8", Value: "NvarcharValue" }
                ],
                "dbType": "VarcharValue",
                "dbtypeName": "Text Data UTF8",
            },
            "className": "formly-control-big",
            "type": "savis-upload",
            "key": "upload",
            "templateOptions": {
                "apiUrl": "http://210.245.26.130:8800/api/v1/core/nodes/upload?AppId=48ed5b71-66dc-4725-9604-4c042e45fa3f&UserId=7efa183f-0013-43d6-a972-29abd7a93bc1&CreateByUser=savis_admin&DestinationPath=ARM-Temp/2017/Van-Ban/Tai-Lieu-Dinh-Kem/10/3&IsScanvirus=false&[[Authencation:RUNNX0FVVEhfQVBJOnNlY3JldA==]]",
                "returnProp": "data.Data",
                "uploadText": 'Upload',
                "accept": "image/*,audio/*",
                "maxsize": "2GB",
                "nameAtribute": "FileName",
                "sizeAtribute": "FileSizeInBytes",
                "isUseHideExpression": false,
                "hideExpression": "",
                "multiUpload": false,
                "isUseDisabledExpression": false,
                "disabledExpression": "",
            }
        };
        factory.UploadMultiControl = {
            "data": {
                "type": 0,
                "name": "Upload Multi",
                "key": "UploadMultiControl",
                "group": "upload",

                "availableDbType": [
                    { Name: "Text Data UTF8", Value: "NvarcharValue" }
                ],
                "dbType": "VarcharValue",
                "dbtypeName": "Text Data UTF8",
            },
            "className": "formly-control-big",
            "type": "savis-upload",
            "key": "upload",
            "templateOptions": {
                "apiUrl": "http://210.245.26.130:8800/api/v1/core/nodes/upload?AppId=48ed5b71-66dc-4725-9604-4c042e45fa3f&UserId=7efa183f-0013-43d6-a972-29abd7a93bc1&CreateByUser=savis_admin&DestinationPath=ARM-Temp/2017/Van-Ban/Tai-Lieu-Dinh-Kem/10/3&IsScanvirus=false&[[Authencation:RUNNX0FVVEhfQVBJOnNlY3JldA==]]",
                "returnProp": "data.Data",
                "uploadText": 'Tải lên',
                "accept": "image/*,audio/*",
                "maxsize": "2GB",
                "nameAtribute": "FileName",
                "sizeAtribute": "FileSizeInBytes",
                "isUseHideExpression": false,
                "hideExpression": "",
                "multiUpload": true,
                "isUseDisabledExpression": false,
                "disabledExpression": "",
            }
        };

        factory.CheckboxControl = {
            "data": {
                "type": 0,
                "name": "Checkbox",
                "key": "CheckboxControl",
                "group": "checkbox",

                "availableDbType": [
                    { Name: "Boolean data", Value: "BitValue" }],
                "dbType": "BitValue",
                "dbtypeName": "Boolean data",
            },
            "defaultValue": false,
            "className": "formly-control",
            "type": "savis-checkbox",
            "key": "checkboxControl",
            "templateOptions": {
                "label": "CheckboxControl",

                "isUseHideExpression": false,
                "hideExpression": "",

                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            }
        };

        factory.ToggleControl = {
            "data": {
                "type": 0,
                "name": "Toggle",
                "key": "ToggleControl",

                "availableDbType": [
                    { Name: "Boolean data", Value: "BitValue" }],
                "dbType": "BitValue",
                "dbtypeName": "Boolean data",
            },
            "defaultValue": false,
            "className": "formly-control",
            "type": "toggle",
            "key": "toggleControl",
            "templateOptions": {
                "label": "ToggleControl",
                "trueLabel": "ON",
                "falseLabel": "OFF",
                //hideExpression
                "isUseHideExpression": false, //lựa chọn validate custom
                "hideExpression": "", //câu điều kiện
                //hideExpression
                "isUseDisabledExpression": false, //lựa chọn validate custom
                "disabledExpression": "", //câu điều kiện
            },
        };



        factory.SelectTreeControl = {
            "data": {
                "type": 0,
                "name": "UI-Select-Tree",
                "key": "SelectTreeControl",
                "group": "dropdown",

                "availableDbType": [
                    { Name: "Text Data UTF8", Value: "NvarcharValue" },
                    { Name: "Link data", Value: "GuidValue" }],
                "dbType": "NvarcharValue",
                "dbtypeName": "Text Data UTF8",
                "isCatalog": true,
                "CatalogMasterCode": "NONE"
            },
            "className": "formly-control",
            "key": 'SelectTreeControl',
            "type": 'ui-select-tree',

            "templateOptions": {
                "label": "SelectTreeControl",
                "valueProp": "value",
                "labelProp": "name",
                "groupProp": "group",
                "placeholder": "pick one",
                "options": [{
                    value: "1",
                    name: "Option1"
                }, {
                    value: "2",
                    name: "Option2"
                }],
                "isGetOptionByApi": false,
                "apiGetOption": 'https://jsonplaceholder.typicode.com/comments?postId=$references',
                "apiResponseData": '$response',

                "isUseHideExpression": false,
                "hideExpression": "",

                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            }
        };

        factory.SelectControl = {
            "data": {
                "type": 0,
                "name": "UI-Select",
                "key": "SelectControl",
                "group": "dropdown",

                "availableDbType": [
                    { Name: "Text Data UTF8", Value: "NvarcharValue" },
                    { Name: "Link data", Value: "GuidValue" }],
                "dbType": "NvarcharValue",
                "dbtypeName": "Text Data UTF8",
                "isCatalog": true,
                "CatalogMasterCode": "NONE"
            },
            "className": "formly-control",
            "key": 'SelectControl',
            "type": 'ui-select',

            "templateOptions": {
                "label": "SelectControl",

                "valueProp": "value",
                "labelProp": "name",
                "groupProp": "group",
                "placeholder": "pick one",
                "options": [{
                    value: "1",
                    name: "Option1"
                }, {
                    value: "2",
                    name: "Option2"
                }],
                "isGetOptionByApi": false,
                "apiGetOption": 'https://jsonplaceholder.typicode.com/comments?postId=$references',
                "apiResponseData": '$response',

                "isUseHideExpression": false,
                "hideExpression": "",

                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            }
        };

        factory.SelectMultiControl = {
            "data": {
                "type": 0,
                "name": "UI-Select-Multiple",
                "key": "SelectMultiControl",
                "group": "dropdown",
                "availableDbType": [
                    { Name: "List data", Value: "JsonValue" },
                    { Name: "List data linked", Value: "GuidJsonValue" }],
                "dbType": "JsonValue",
                "dbtypeName": "List data",
                "isCatalog": true,
                "CatalogMasterCode": "NONE"
            },
            "className": "formly-control",

            "key": 'uiSelectMultiControl',
            "type": 'ui-select-multiple',
            "templateOptions": {
                "label": "SelectMultiControl",
                "valueProp": "value",
                "labelProp": "name",
                "groupProp": "group",
                "placeholder": "pick some",

                "options": [{
                    value: "1",
                    name: "Option1"
                }, {
                    value: "2",
                    name: "Option2"
                }],
                "isGetOptionByApi": false,
                "apiGetOption": 'https://jsonplaceholder.typicode.com/comments?postId=$references',
                "apiResponseData": '$response',



                "isUseHideExpression": false,
                "hideExpression": "",

                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            }
        };

        factory.RadioButtonControl = {
            "data": {
                "type": 0,
                "name": "Radio Button",
                "key": "RadioButtonControl",
                "group": "checkbox",
                "availableDbType": [
                    { Name: "Text Data UTF8", Value: "NvarcharValue" },
                    { Name: "Link data", Value: "GuidValue" }],
                "dbType": "NvarcharValue",
                "dbtypeName": "Text Data UTF8",
                "isCatalog": true,
                "CatalogMasterCode": "NONE"
            },

            "className": "formly-control",
            "key": "radio",
            "type": "savis-radioBtn",
            "templateOptions": {
                "label": "RadioButtonControl",
                "valueProp": "value",
                "labelProp": "name",
                "groupProp": "group",
                "inline": true,
                "options": [{
                    value: "1",
                    name: "Option1"
                }, {
                    value: "2",
                    name: "Option2"
                }],
                "isGetOptionByApi": false,
                "apiGetOption": 'https://jsonplaceholder.typicode.com/comments?postId=$references',
                "apiResponseData": '$response',


                "isUseHideExpression": false,
                "hideExpression": "",

                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            }
        };

        factory.CheckboxMultiControl = {
            "data": {
                "type": 0,
                "name": "Checkbox Multi",
                "key": "CheckboxMultiControl",
                "group": "checkbox",
                "availableDbType": [
                    { Name: "List data", Value: "JsonValue" },
                    { Name: "List data linked", Value: "GuidJsonValue" }],
                "dbType": "JsonValue",
                "dbtypeName": "List data",
                "isCatalog": true,
                "CatalogMasterCode": "NONE"
            },

            "className": "formly-control-big",
            "key": "checkboxMultiControl",
            "type": "savis-checkboxMulti",
            "templateOptions": {
                "label": "CheckboxMultiControl",
                "valueProp": "value",
                "labelProp": "name",
                "groupProp": "group",
                "inline": true,
                "options": [{
                    value: "1",
                    name: "Option1"
                }, {
                    value: "2",
                    name: "Option2"
                }],
                "isGetOptionByApi": false,
                "apiGetOption": 'https://jsonplaceholder.typicode.com/comments?postId=$references',
                "apiResponseData": '$response',


                "isUseHideExpression": false,
                "hideExpression": "",

                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            }
        };

        factory.CheckboxMultiTreeControl = {
            "data": {
                "type": 0,
                "name": "Checkbox Multi Tree",
                "key": "CheckboxMultiTreeControl",
                "group": "checkbox",
                "availableDbType": [
                    { Name: "List data", Value: "JsonValue" },
                    { Name: "List data linked", Value: "GuidJsonValue" }],
                "dbType": "JsonValue",
                "dbtypeName": "List data",
                "isCatalog": true,
                "CatalogMasterCode": "NONE"
            },
            "className": "formly-control-big",
            "key": "checkboxMultiTreeControl",
            "type": "savis-checkboxMultiTree",
            "templateOptions": {
                "label": "CheckboxMultiTreeControl",
                "valueProp": "value",
                "labelProp": "name",
                "groupProp": "group",
                "inline": true,
                "options": [{
                    value: "1",
                    name: "Option1"
                }, {
                    value: "2",
                    name: "Option2"
                }],
                "isGetOptionByApi": false,
                "apiGetOption": 'https://jsonplaceholder.typicode.com/comments?postId=$references',
                "apiResponseData": '$response',


                "isUseHideExpression": false,
                "hideExpression": "",

                "isUseDisabledExpression": false,
                "disabledExpression": "",
                "horizontalLabel": false,
                "labelSize": "30%",
                "controlSize": "70%",
            }
        };


        factory.TableControl = {
            "data": {
                "type": 0,
                "name": "Table",
                "key": "TableControl",
                "group": "table",
                "availableDbType": [
                    { Name: "List data", Value: "JsonValue" }],
                "dbType": "JsonValue",
                "dbtypeName": "List data",
            },
            "className": "formly-control-big",
            "type": "savis-ag-grid",
            "key": "table",
            "templateOptions": {
                "headerConfig": [{"headerName":"Number data","type":"numberColumn","field":"Number"},{"headerName":"Text Data UTF8","type":"textColumn","field":"Text"},{"headerName":"Date time data","type":"dateColumn","field":"Date"}],
            }
        };

        factory.ListAvailAbleControl = [];
        factory.ListAvailAbleControl.push({ Name: "InputTextControl", "Group": "Input", Value: factory.InputTextControl });
        factory.ListAvailAbleControl.push({ Name: "InputEmailControl", "Group": "Input", Value: factory.InputEmailControl });
        factory.ListAvailAbleControl.push({ Name: "InputCodeControl", "Group": "Input", Value: factory.InputCodeControl });
        factory.ListAvailAbleControl.push({ Name: "InputUrlControl", "Group": "Input", Value: factory.InputUrlControl });
        factory.ListAvailAbleControl.push({ Name: "InputPasswordControl", "Group": "Input", Value: factory.InputPasswordControl });
        factory.ListAvailAbleControl.push({ Name: "InputColorControl", "Group": "Input", Value: factory.InputColorControl });
        /*Bind level------------------------------------------------------------*/
        factory.ListAvailAbleControl.push({ Name: "InputNumberControl", "Group": "Number", Value: factory.InputNumberControl });
        factory.ListAvailAbleControl.push({ Name: "InputRangeControl", "Group": "Number", Value: factory.InputRangeControl });

        factory.ListAvailAbleControl.push({ Name: "InputDateFullControl", "Group": "DateTime", Value: factory.InputDateFullControl });
        factory.ListAvailAbleControl.push({ Name: "InputDateControl", "Group": "DateTime", Value: factory.InputDateControl });
        factory.ListAvailAbleControl.push({ Name: "InputYearControl", "Group": "DateTime", Value: factory.InputYearControl });
        factory.ListAvailAbleControl.push({ Name: "InputMonthControl", "Group": "DateTime", Value: factory.InputMonthControl });
        factory.ListAvailAbleControl.push({ Name: "InputTimeControl", "Group": "DateTime", Value: factory.InputTimeControl });
        /*Bind level------------------------------------------------------------*/
        factory.ListAvailAbleControl.push({ Name: "CheckboxControl", "Group": "CheckBox", Value: factory.CheckboxControl });
        factory.ListAvailAbleControl.push({ Name: "ToggleControl", "Group": "CheckBox", Value: factory.ToggleControl });
        /*Bind level------------------------------------------------------------*/
        factory.ListAvailAbleControl.push({ Name: "TextAreaControl", "Group": "TextArea", Value: factory.TextAreaControl });
        factory.ListAvailAbleControl.push({ Name: "CkeditorControl", "Group": "TextArea", Value: factory.CkeditorControl });
        /*Bind level------------------------------------------------------------*/
        factory.ListAvailAbleControl.push({ Name: "UploadControl", "Group": "Upload", Value: factory.UploadControl });
        factory.ListAvailAbleControl.push({ Name: "UploadMultiControl", "Group": "Upload", Value: factory.UploadMultiControl });
        /*Bind level------------------------------------------------------------*/
        factory.ListAvailAbleControl.push({ Name: "SelectTreeControl", "Group": "Selector", Value: factory.SelectTreeControl });
        factory.ListAvailAbleControl.push({ Name: "SelectControl", "Group": "Selector", Value: factory.SelectControl });
        factory.ListAvailAbleControl.push({ Name: "SelectMultiControl", "Group": "Selector", Value: factory.SelectMultiControl });
        factory.ListAvailAbleControl.push({ Name: "RadioButtonControl", "Group": "Selector", Value: factory.RadioButtonControl });
        factory.ListAvailAbleControl.push({ Name: "CheckboxMultiControl", "Group": "Selector", Value: factory.CheckboxMultiControl });
        factory.ListAvailAbleControl.push({ Name: "CheckboxMultiTreeControl", "Group": "Selector", Value: factory.CheckboxMultiTreeControl });
        /*Bind level------------------------------------------------------------*/
        factory.ListAvailAbleControl.push({ Name: "TableControl", "Group": "table", Value: factory.TableControl });

        factory.ListAvailAbleDbType = [
            { Name: "Number data", Value: "IntValue" },
            { Name: "Boolean data", Value: "BitValue" },
            { Name: "Text Data UTF8", Value: "NvarcharValue" },
            { Name: "Text Data", Value: "VarcharValue" },
            { Name: "Date time data", Value: "datetime" },
            { Name: "Link data", Value: "GuidValue" },
        ];
        factory.ListAvailAbleClass = [
            "col-md-1",
            "col-md-2",
            "col-md-3",
            "col-md-4",
            "col-md-5",
            "col-md-6",
            "col-md-7",
            "col-md-8",
            "col-md-9",
            "col-md-10",
            "col-md-11",
            "formly-control",
        ];

        return factory;

    });
    app.service('FormlyService', function () {
        var factory = {};
        factory.NewGuid = function () {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        };

        factory.SerializeJSON = function (jsonObj) {
            return JSON.stringify(jsonObj, function (key, value) {
                if (typeof value === "function") {
                    return value.toString();;
                }
                return value;
            }, 4);
        };

        factory.DeSerializeJSON = function (jsonString) {
            return JSON.parse(jsonString, function (key, value) {
                if (value &&
                    typeof value === "string" &&
                    value.substr(0, 8) == "function") {
                    var startBody = value.indexOf('{') + 1;
                    var endBody = value.lastIndexOf('}');
                    var startArgs = value.indexOf('(') + 1;
                    var endArgs = value.indexOf(')');
                    var param = value.substring(startArgs, endArgs);
                    var body = value.substring(startBody, endBody);
                    return eval("(" + value + ")");
                }
                return value;
            });
        };

        return factory;

    });
});


